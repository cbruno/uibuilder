//= require AppKit/js/AppKit.js
//= require ./UIBuilder.js
//= require ./UIBuilder.Components.js
//= require ./UIBuilder.EditorView.js



UIBuilder.AppController = { 
   
};

UIBuilder.EditorViewController = {
	createEmptyViewFile : function()
	{
		this.view.setEditedView(
				new UIBuilder.DesignEditFrame({
					component : new UIBuilder.DesignerView()
				 })); 
	},
	removeAllEditMasks : function()
	{
		this.view.removeAllEditMasks();
	},
	showEditMask : function(editFrame)
	{
		this.view.showEditMask(editFrame);
		
		
	}
};

AppKit.NotificationCenter.addObserver({
	observer : UIBuilder.EditorViewController, 
	action : function(notif){
		this.showEditMask(notif.sender);
	}, 
	name : 'ShowEditMaskNotification' 
});

AppKit.NotificationCenter.addObserver({
	observer : UIBuilder.EditorViewController, 
	action : function(notif){
		this.removeAllEditMasks();
	}, 
	name : 'EndEditMaskNotification' 
});

 



AppKit.launch(function(){
    UIBuilder.EditorViewController.createEmptyViewFile();
    
});
 
