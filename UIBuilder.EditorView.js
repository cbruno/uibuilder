//= require ./UIBuilder.js
//= require ./UIBuilder.EditFrame.js
//= require ./UIBuilder.DesignEditFrame.js
//= require ./UIBuilder.EditableCustomView.js
//= require ./plugins/UIBuilder.EditableButton.js
//= require ./plugins/UIBuilder.EditableLabel.js
//= require ./plugins/UIBuilder.EditableSlider.js
//= require ./plugins/UIBuilder.EditableTextField.js
//= require ./plugins/UIBuilder.EditableSearchField.js

UIBuilder.EditorView = AppKit.View.extend({
	classID : 'uibuilder-editor-view', 
	initialize : function(options)
	{
		var evtID = _.uniqueId('evt-');
		var self = this;
		this.$el.find('.center-container').on('mousedown', function(evt1){
			//start grouping box
			
			AppKit.NotificationCenter.post({
                name : 'EndEditMaskNotification' 
            });

            AppKit.NotificationCenter.post({
				name : 'UnselectComponentsNotification',
				sender : null
			}); 

			evt1.preventDefault(); 
			if(evt1.which < 2){
				var offset = $(this).offset(); 

				var startX = evt1.pageX - offset.left,
					startY = evt1.pageY - offset.top;

				var boxDiv = $('<div>').addClass('uibuilder-grouping-box')
						     .css({
						     	left : startX,
						     	top : startY,
						     	width : 0,
						     	height : 0
						     })
							 .appendTo($(this));

				UIBuilder.GroupDraggingManager.groupedComponents = [];

				$(document).on('mousemove.'+evtID, function(evt2){
					
					if(boxDiv){

						var endX = evt2.pageX - offset.left,
							endY = evt2.pageY - offset.top;

						var left = Math.min(startX, endX),
							top = Math.min(startY, endY);

						boxDiv.css({
							left : left,
							top : top,
							width : Math.abs(endX - startX),
							height : Math.abs(endY - startY)
						});

						UIBuilder.GroupDraggingManager.groupedComponents = [];
						self.currentView.component.$el.children('.uibuilder-editframe').each(function(idx, elem ){
							 var editFrame = $(elem).widget();
							 var intersects = boxDiv.intersectsEl($(elem));
							 editFrame.set('selected', intersects);
							 if(intersects){
							 	UIBuilder.GroupDraggingManager.groupedComponents.push(editFrame); 
							 }
						});
					}

				
				}).on('mouseup.'+evtID, function(){
					$(document).off('mousemove.'+evtID + ' mouseup.'+evtID);
					boxDiv.remove();
					boxDiv = null; 
				});
			}
		});
	},
	setEditedView : function(aView){
		if(aView){
			this.currentView = aView;
			UIBuilder.DDManager.mainView = this.currentView.component;
			this.currentView.$el.appendTo(this.$el.find('.center-container'));
		}
	},
	showEditMask : function(editFrame)
	{
		 AppKit.NotificationCenter.post({
            name : 'EndEditMaskNotification' 
         });

		if(!editFrame.get('editMask')){
			var editMask = new UIBuilder.EditMask({
				editFrame : editFrame
			});
			
			editMask.$el.appendTo(this.$el.find('.uibuilder-design-view'));

			editMask.show();
		}
	},
	removeAllEditMasks : function()
	{
		this.$el.find('.uibuilder-edit-mask').each(function(idx, elem){
			var editMask = $(elem).widget();
			if(editMask){
				editMask.remove();
			}
		});
	}
}); 
