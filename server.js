#!/usr/bin/env node
'use strict';
 
var server = require('./AppKit/tools/server.js');

var app = server.createAppServer();



var server = app.listen(3000, function () {

  var port = server.address().port
  console.info('Server listening at localhost:%s', port);
  console.info('Press CTRL-C to exit.');

});