
 

UIBuilder.DDManager = {
	mainView : null,
	initDrag : function(el){
		this._draggable = el;
		this._startingTopView = this.topViewUnderDraggable();
	},
	endDrag : function(){
		this._draggingFrame = null;
		this._startingTopView = null;
		if(this.mainView){
			this.mainView.$el.find('.uibuilder-custom-view').removeClass('droppable-over');
		}
	},
	highlightDropView : function(){

		var topView = this.topViewUnderDraggable(); 
			
		if(topView !== this._startingTopView){
			AppKit.NotificationCenter.post({
            	name : 'EndEditMaskNotification' 
         	});
		}

		if(topView && topView !== this.mainView){
			var editFrame = topView.get('editFrame');
			if(!editFrame.get('editMode')){
				editFrame.set('selected', false);
				topView.$el.addClass('droppable-over');
			}
		}
	},
	topViewUnderDraggable : function()
	{
		if(this.mainView && this._draggable){
			var self = this,
				topViewEl = null;
			this.mainView.$el.find('.uibuilder-custom-view')
							 .not(self._draggable.find('.uibuilder-custom-view'))
							 .each(function(idx, el){
				
				$(el).removeClass('droppable-over');
				if(self.intersectionTest(self._draggable, $(el))){
					topViewEl = $(el);
				}
			});

			if(!topViewEl){
				return this.mainView;
			}

			return topViewEl.widget();
		}
	}, 
	dropComponentElementIntoView : function(componentEl, view, dropInfo){

		var wAdj = (view.$el.outerWidth() - view.$el.innerWidth())/2.0,
		 	hAdj = (view.$el.outerHeight() - view.$el.innerHeight())/2.0;
		
		if(componentEl.hasClass('component-item')){
	 		 
	 		 var componentID = componentEl.data('componentID'),
		 	 	 component = UIBuilder.Components.create(componentID);
			 
		 	 var editFrame = null;
			 if(componentID === 'view'){
			 	editFrame = new UIBuilder.EditViewFrame( 
			 		_.extend({component : component}, component.editOptions)
			 	 );
			 }
			 else {
			 	 editFrame = new UIBuilder.EditFrame( 
			 		_.extend({component : component}, component.editOptions)
			 	);
			 }
			 
			editFrame.$el.appendTo(view.$el);
			editFrame.component.superview = view; 

			editFrame.$el.css({
			 	left : dropInfo.offset.left - view.$el.offset().left - wAdj  ,
			 	top : dropInfo.offset.top - view.$el.offset().top   ,
			 	width : component.$el.outerWidth(),
			 	height : component.$el.outerHeight()
			 });

			 editFrame.sizer();
			 editFrame.$el.focus();

		 }
		 else
		 {
		 	var cOffset = componentEl.offset(),
				vOffset = view.$el.offset();  
		 	
		 	componentEl.css({
		 		left : cOffset.left - vOffset.left - wAdj,
		 		top : cOffset.top -  vOffset.top - hAdj
			}); 

		 	componentEl.appendTo(view.$el); 
		 	 
		 	var component = componentEl.widget();
		 	if(component){
		 		component.superview = view;
		 	}
	 	 	
		 	if(UIBuilder.GroupDraggingManager.groupedComponents.length){
		 		UIBuilder.GroupDraggingManager.completeDrop(view);
		 	} 

	 	 	componentEl.focus(); 
		 }
	},
	intersectionTest : function(draggable, droppable){

			var p1 = draggable.offset(),
				p2 = droppable.offset(),
				w1 = draggable.width(),
				w2 = droppable.width(),
				h1 = draggable.height(),
				h2 = droppable.height(); 

		    var x1 = p1.left,
                y1 = p1.top,
                x2 = x1 + w1,
                y2 = y1 + h1,
                l = p2.left,
                t = p2.top,
                r = l + w2,
                b = t + h2;

		return ( l < x1 + ( w1 / 2 ) && // Right Half
                        x2 - ( w1 / 2 ) < r && // Left Half
                        t < y1 + ( h1 / 2 ) && // Bottom Half
                        y2 - ( h1 / 2 ) < b ); // Top Half
	}

}