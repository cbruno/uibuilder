//= require ./UIBuilder.EditableCustomView.js
//= require ./UIBuilder.DDManager.js

UIBuilder.DesignEditFrame = UIBuilder.EditFrame.extend({
	className : 'uibuilder-design-frame', 
	options : {
		leftResizer : false,
		rightResizer : false,
		topResizer : false,
		bottomResizer : false,
		topLeftResizer : false,
		bottomLeftResizer : false,
		topRightResizer : false,
		bottomRightResizer : false,
		editValue : false
	},
	initialize : function(options){
		this._super(options);
		this.$el.draggable('destroy'); 
	
		var self = this;
		this.$el.droppable({
			greedy : true,
			accept : '.uibuilder-editframe, .component-item',
			drop : function(event, ui){
				
				var topView = UIBuilder.DDManager.topViewUnderDraggable();
				if(topView){
					UIBuilder.DDManager.dropComponentElementIntoView(ui.draggable, topView, ui);
				} 
			}
		});
	},
	_onMouseDown : function(evt){
		this.$el.focus();
	},
	_onFocus : function(evt){
		
		AppKit.NotificationCenter.post({
			name : 'UnselectComponentsNotification',
			sender : null
		});

		this.set('selected', true); 
		
		UIBuilder.GroupDraggingManager.groupedComponents = [];
	} 
});


UIBuilder.DesignerView = UIBuilder.EditableCustomView.extend({
	className : 'uibuilder-design-view',
	initialize : function(options){
		this._super(options);

		this.$el.find('.table-container').remove(); 

	} 
});


jQuery(function(){

	$(document).on('keydown', function(evt){
			
			switch(evt.keyCode){
				case $.ui.keyCode.DELETE:
				{	
					$(this).find('.ui-state-selected').remove();				
				}break;
				case $.ui.keyCode.BACKSPACE:
				{
					evt.preventDefault();
					$(this).find('.ui-state-selected').remove();
				}break;
				case $.ui.keyCode.RIGHT: {	
					
					$(this).find('.ui-state-selected').each(function(idx, elem){
						$(elem).css('left', $(elem).position().left+1);
					});

				}break;
				case $.ui.keyCode.LEFT: {
					$(this).find('.ui-state-selected').each(function(idx, elem){
						$(elem).css('left', $(elem).position().left-1);
					});
				}break;
				case $.ui.keyCode.UP: {
					$(this).find('.ui-state-selected').each(function(idx, elem){
						$(elem).css('top', $(elem).position().top-1);
					});
				}break;
				case $.ui.keyCode.DOWN: {
					$(this).find('.ui-state-selected').each(function(idx, elem){
						$(elem).css('top', $(elem).position().top+1);
					});
				}break;
			} 
		}
	); 

});


