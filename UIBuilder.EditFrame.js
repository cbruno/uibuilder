//= require ./UIBuilder.EditMask.js
//= require ./UIBuilder.GroupDraggingManager.js
//= require ./UIBuilder.GuidelinesManager.js


UIBuilder.EditFrame = AppKit.View.extend({
	className : 'uibuilder-editframe',
	component : null,
	options : {
		leftResizer : false,
		rightResizer : false,
		topResizer : false,
		bottomResizer : false,
		topLeftResizer : false,
		bottomLeftResizer : false,
		topRightResizer : false,
		bottomRightResizer : false,
		editValue : true,
		singleLineEdit : true
	},
	initialize : function(options){

		this._super(options);

		this.$el.attr('tabindex', -1);

		this.component.$el.css({
			left : 0,
			top : 0
		});

		this.component.$el.addClass('component');
		 
		this.component.$el.appendTo(this.$el);
		this.component.$el.off('mousedown click');
		this.component.$el.children().off('mousedown click');
 	
 		this.component.set('editFrame', this);

 		//add in selected mask
 		$('<div>').addClass('uibuilder-editframe-selectmask')
 				.addClass(this.component.classID)
 				.appendTo(this.$el); 

 		var self = this;
		this.$el.draggable({
			scroll : false,
			revert : function(invalid){

				if(!invalid){
					if(UIBuilder.GroupDraggingManager.groupedComponents.length){
						UIBuilder.GroupDraggingManager.revertDrag();
						return invalid;
					} 
				} 
				return !invalid;
			},
			stack  : '.uibuilder-editframe', 
			delay: 100,
			start : function(evt, ui){ 

				if(UIBuilder.GroupDraggingManager.groupedComponents.length){
					UIBuilder.GroupDraggingManager.startDrag();
				} 

				self.set('selected', false); 

				UIBuilder.DDManager.initDrag(self.$el);
				UIBuilder.GuidLinesManager.initWithEditFrame(self);

			},
			stop : function(evt, ui){

				UIBuilder.DDManager.endDrag();
			},
			drag : function(evt, ui){

				if(_.contains(UIBuilder.GroupDraggingManager.groupedComponents, self)){
				 	var delta = {
			 			x : ui.position.left - ui.originalPosition.left,
			 			y : ui.position.top - ui.originalPosition.top
				 	};

				 	UIBuilder.GroupDraggingManager.move(delta); 

				 	ui.originalPosition.left = ui.position.left;
				 	ui.originalPosition.top = ui.position.top;
				}

				UIBuilder.DDManager.highlightDropView();

				//UIBuilder.GuidLinesManager.detect();

			}
		});
 
		this._bindResizeEvent();

		AppKit.NotificationCenter.addObserver({
			observer : this, 
			action : function(){
				this.set('selected', false);
				UIBuilder.GroupDraggingManager.groupedComponents = [];
			}, 
			name : 'UnselectComponentsNotification' 
		}); 
	},
	events : {
		focus : '_onFocus',
		mousedown : '_onMouseDown',
		dblclick : '_onDblClick'	
	},
	selected : function(aBool){
		if(aBool === true){
			this.$el.addClass('ui-state-selected');
			this.$el.children('.edit-resizer').show();
			this.$el.children('.uibuilder-editframe-selectmask').show();

			AppKit.NotificationCenter.post({
                name : 'EndEditMaskNotification' 
            });

			//highlight frame
			var parent = this.$el.parent();
			if(parent.hasClass('uibuilder-custom-view')){

				var viewFrame = parent.parent().widget();
				
				if(viewFrame && !viewFrame.get('editMode')){
					AppKit.NotificationCenter.post({
						name : 'ShowEditMaskNotification',
						sender : viewFrame
					}); 
				}  
			}

		}
		else if(aBool === false){
			this.$el.removeClass('ui-state-selected');
			this.$el.children('.edit-resizer').hide();
			this.$el.children('.uibuilder-editframe-selectmask').hide();  
		}

		return this.$el.hasClass('ui-state-selected');
	},
	editor : function()
	{
		var editor = $('<div class="uibuilder-component-edit" ><textarea  /></div>')
						.addClass(this.component.classID);

			
		var self = this;
		editor.children('textarea').on('mousedown click keydown', function(evt){
			evt.stopPropagation();
		}).on('blur', function(){ 
			
			if(self.get('editMask')){
				self.get('editMask').remove();
			}

			if(_.isFunction(self.component.edit)){
				self.component.edit($(this).val().replace(/\n/g, ""));
			}

		}).on('keypress', function(evt){
			if(evt.keyCode == $.ui.keyCode.ENTER) {
				evt.preventDefault();
                self.$el.focus();
            }
		});

		editor.css({
			width : this.$el.width()+6,
			height : this.$el.height()+6
		});

		return editor;
	},
	sizer : function()
	{
		if(this._leftResizer){
			this._leftResizer.css({
				top : Math.floor((this.$el.height() - 8)/2.0)
			});
		}
		
		if(this._rightResizer){
			this._rightResizer.css({
				top : Math.floor((this.$el.height() - 8)/2.0)
			});
		}
		
		if(this._topResizer){
			this._topResizer.css({
				left : Math.round((this.$el.width() - 8)/2.0)
			});
		}
		
		if(this._bottomResizer){
			this._bottomResizer.css({
				left : Math.round((this.$el.width() - 8)/2.0)
			});
		} 
		
	},
	leftResizer : function(aBool){
		if(aBool === true){
			if(!this._leftResizer){
				this._leftResizer = $('<div>').addClass('edit-resizer left').appendTo(this.$el);
			} 
		}
		else if(aBool === false){
			if(this._leftResizer){
				this._leftResizer.remove();
				this._leftResizer = null;  
			} 
		}

		return this._leftResizer;
	},
	rightResizer : function(aBool){
		if(aBool === true){
			if(!this._rightResizer){
				this._rightResizer = $('<div>').addClass('edit-resizer right').appendTo(this.$el);
			} 
		}
		else if(aBool === false){
			if(this._rightResizer){
				this._rightResizer.remove();
				this._rightResizer = null;  
			} 
		}

		return this._rightResizer;
	},
	topResizer : function(aBool){
		if(aBool === true){
			if(!this._topResizer){
				this._topResizer = $('<div>').addClass('edit-resizer top').appendTo(this.$el);
			} 
		}
		else if(aBool === false){
			if(this._topResizer){
				this._topResizer.remove();
				this._topResizer = null;  
			} 
		}

		return this._topResizer;
	},
	bottomResizer : function(aBool){
		if(aBool === true){
			if(!this._bottomResizer){
				this._bottomResizer = $('<div>').addClass('edit-resizer bottom').appendTo(this.$el);
			} 
		}
		else if(aBool === false){
			if(this._bottomResizer){
				this._bottomResizer.remove();
				this._bottomResizer = null;  
			} 
		}

		return this._bottomResizer;		
	},
	topLeftResizer : function(aBool){

		if(aBool === true){
			if(!this._topLeftResizer){
				this._topLeftResizer = $('<div>').addClass('edit-resizer topleft').appendTo(this.$el);
			} 
		}
		else if(aBool === false){
			if(this._topLeftResizer){
				this._topLeftResizer.remove();
				this._topLeftResizer = null;  
			} 
		}

		return this._topLeftResizer;	
	},
	bottomLeftResizer : function(aBool){

		if(aBool === true){
			if(!this._bottomLeftResizer){
				this._bottomLeftResizer = $('<div>').addClass('edit-resizer bottomleft').appendTo(this.$el);
			} 
		}
		else if(aBool === false){
			if(this._bottomLeftResizer){
				this._bottomLeftResizer.remove();
				this._bottomLeftResizer = null;  
			} 
		}

		return this._bottomLeftResizer;	
	},
	topRightResizer : function(aBool){

		if(aBool === true){
			if(!this._topRightResizer){
				this._topRightResizer = $('<div>').addClass('edit-resizer topright').appendTo(this.$el);
			} 
		}
		else if(aBool === false){
			if(this._topRightResizer){
				this._topRightResizer.remove();
				this._topRightResizer = null;  
			} 
		}

		return this._topRightResizer;	
	},
	bottomRightResizer : function(aBool){

		if(aBool === true){
			if(!this._bottomRightResizer){
				this._bottomRightResizer = $('<div>').addClass('edit-resizer bottomright').appendTo(this.$el);
			} 
		}
		else if(aBool === false){
			if(this._bottomRightResizer){
				this._bottomRightResizer.remove();
				this._bottomRightResizer = null;  
			}
		}

		return this._bottomRightResizer;	
	},
	_bindResizeEvent : function()
	{	
		var evtID = _.uniqueId('evt-');
		var self = this;
		this.$el.find('.edit-resizer').on('mousedown', function(mdEvt){
			mdEvt.preventDefault();
			mdEvt.stopPropagation();
			self._isResizing = true;

			var initLeft = new Number(self.$el.offset().left).valueOf(),
				initTop = new Number(self.$el.offset().top).valueOf(),
				initMaxX = new Number(self.$el.offset().left + self.$el.width()).valueOf(),
				initMaxY = new Number(self.$el.offset().top + self.$el.height()).valueOf(); 
			
			var resizer = $(this);
 
			$(document).on('mousemove.'+evtID, function(evt){
				//do resizing
				var newW = -1,
					newH = -1;
				if(resizer.hasClass('right')){
					newW = evt.pageX - initLeft;
					if(newW < 0){
						var l = evt.pageX - self.$el.parent().offset().left;
						self.$el.css('left', l); 
						newW = initLeft -  self.$el.offset().left;  
					}
					self.$el.parent().css('cursor', 'ew-resize');
				}
				else if(resizer.hasClass('left')){
					
					if(initMaxX - self.$el.offset().left  > 0){
						var l = Math.min(initMaxX, evt.pageX) - self.$el.parent().offset().left;
						self.$el.css('left', l);
						newW = initMaxX - self.$el.offset().left;  
					}
					else {
						
						newW = evt.pageX - self.$el.offset().left;
						if(newW < 0){
							var l = evt.pageX - self.$el.parent().offset().left;
							self.$el.css('left', Math.min(l, initMaxX- self.$el.parent().offset().left));
							newW = initMaxX - self.$el.offset().left;  
						} 

					} 

					self.$el.parent().css('cursor', 'ew-resize');
				}
				else if(resizer.hasClass('top')){
					
					if(initMaxY - self.$el.offset().top  > 0){
						var t = evt.pageY - self.$el.parent().offset().top;
						self.$el.css('top', t);
						newH = initMaxY - self.$el.offset().top;  
					}
					else {
						
						newH = evt.pageY - self.$el.offset().top;
						if(newH < 0){
							var t = evt.pageY - self.$el.parent().offset().top;
							self.$el.css('top', Math.min(t, initMaxY- self.$el.parent().offset().top));
							newH = initMaxY - self.$el.offset().top;  
						} 

					} 

					self.$el.parent().css('cursor', 'ns-resize');
				}
				else if(resizer.hasClass('bottom')){
					newH = evt.pageY - initTop;
					if(newH < 0){
						var maxY = self.$el.offset().top + self.$el.height();
						var t = Math.min(initTop, evt.pageY) - self.$el.parent().offset().top;
						self.$el.css('top', t); 
						newH = maxY - self.$el.offset().top;  
					}
					self.$el.parent().css('cursor', 'ns-resize');
				}
				else if(resizer.hasClass('bottomright')){
					newH = evt.pageY - initTop;
					if(newH < 0){
						var maxY = self.$el.offset().top + self.$el.height();
						var t = Math.min(initTop, evt.pageY) - self.$el.parent().offset().top;
						self.$el.css('top', t); 
						newH = maxY - self.$el.offset().top;  
					}
					newW = evt.pageX - initLeft;
					if(newW < 0){
						var l = evt.pageX - self.$el.parent().offset().left;
						self.$el.css('left', l); 
						newW = initLeft -  self.$el.offset().left;  
					}
					self.$el.parent().css('cursor', 'nwse-resize');
				}
				else if(resizer.hasClass('topright')){
					if(initMaxY - self.$el.offset().top  > 0){
						var t = evt.pageY - self.$el.parent().offset().top;
						self.$el.css('top', t);
						newH = initMaxY - self.$el.offset().top;  
					}
					else {
						
						newH = evt.pageY - self.$el.offset().top;
						if(newH < 0){
							var t = evt.pageY - self.$el.parent().offset().top;
							self.$el.css('top', Math.min(t, initMaxY- self.$el.parent().offset().top));
							newH = initMaxY - self.$el.offset().top;  
						} 

					}  
					newW = evt.pageX - initLeft;
					if(newW < 0){
						var l = evt.pageX - self.$el.parent().offset().left;
						self.$el.css('left', l); 
						newW = initLeft -  self.$el.offset().left;  
					}

					self.$el.parent().css('cursor', 'nesw-resize');
				}
				else if(resizer.hasClass('topleft')){
					if(initMaxY - self.$el.offset().top  > 0){
						var t = evt.pageY - self.$el.parent().offset().top;
						self.$el.css('top', t);
						newH = initMaxY - self.$el.offset().top;  
					}
					else {
						
						newH = evt.pageY - self.$el.offset().top;
						if(newH < 0){
							var t = evt.pageY - self.$el.parent().offset().top;
							self.$el.css('top', Math.min(t, initMaxY- self.$el.parent().offset().top));
							newH = initMaxY - self.$el.offset().top;  
						} 

					} 
					if(initMaxX - self.$el.offset().left  > 0){
						var l = Math.min(initMaxX, evt.pageX) - self.$el.parent().offset().left;
						self.$el.css('left', l);
						newW = initMaxX - self.$el.offset().left;  
					}
					else {
						
						newW = evt.pageX - self.$el.offset().left;
						if(newW < 0){
							var l = evt.pageX - self.$el.parent().offset().left;
							self.$el.css('left', Math.min(l, initMaxX- self.$el.parent().offset().left));
							newW = initMaxX - self.$el.offset().left;  
						} 

					} 
					self.$el.parent().css('cursor', 'nwse-resize');
				}
				else if(resizer.hasClass('bottomleft')){
					newH = evt.pageY - initTop;
					if(newH < 0){
						var maxY = self.$el.offset().top + self.$el.height();
						var t = Math.min(initTop, evt.pageY) - self.$el.parent().offset().top;
						self.$el.css('top', t); 
						newH = maxY - self.$el.offset().top;  
					}
					if(initMaxX - self.$el.offset().left  > 0){
						var l = Math.min(initMaxX, evt.pageX) - self.$el.parent().offset().left;
						self.$el.css('left', l);
						newW = initMaxX - self.$el.offset().left;  
					}
					else {
						
						newW = evt.pageX - self.$el.offset().left;
						if(newW < 0){
							var l = evt.pageX - self.$el.parent().offset().left;
							self.$el.css('left', Math.min(l, initMaxX- self.$el.parent().offset().left));
							newW = initMaxX - self.$el.offset().left;  
						} 
					} 
					self.$el.parent().css('cursor', 'nesw-resize');
				}


				if(newW > -1){
					self.$el.outerWidth(newW);
					self.component.$el.outerWidth(newW);
				}
				if(newH > -1){
					self.$el.outerHeight(newH);
					self.component.$el.outerHeight(newH);
				}

				self.sizer();

			}).on('mouseup.'+evtID, function(){
				$(document).off('mousemove.'+evtID + ' mouseup.'+evtID);
				self.$el.parent().css('cursor', 'inherit');
				self._isResizing = false; 
			});
		}).on('dblclick', function(evt){ evt.stopPropagation();});

		this.$el.find('.edit-resizer').hide();

	},
	_onMouseDown : function(evt){
		
		evt.stopPropagation();
		
		if(evt.shiftKey){
			this.set('selected', true);
			UIBuilder.GroupDraggingManager.groupedComponents.push(this);

		}
		else
		{
			this.$el.focus();
		}
		
	},
	_onFocus : function(evt){
		
		if(!_.contains(UIBuilder.GroupDraggingManager.groupedComponents, this))
		{

			AppKit.NotificationCenter.post({
				name : 'UnselectComponentsNotification',
				sender : null
			});

			this.set('selected', true);

			UIBuilder.GroupDraggingManager.groupedComponents = [this];
		}
	},
	_onDblClick : function(evt){

		if(this.get('editValue')){
			evt.stopPropagation();

			AppKit.NotificationCenter.post({
				name : 'UnselectComponentsNotification',
				sender : null
			}); 

			AppKit.NotificationCenter.post({
				name : 'ShowEditMaskNotification',
				sender : this,
			}); 
		}
	} 
});


UIBuilder.EditViewFrame = UIBuilder.EditFrame.extend({
	className : 'uibuilder-view-editframe uibuilder-editframe',
	initialize : function(options){
		this._super(options);

		var self = this;
		this.$el.on('dblclick', function(evt){
			evt.stopPropagation();
			
			AppKit.NotificationCenter.post({
				name : 'EndEditMaskNotification' 
			});

			AppKit.NotificationCenter.post({
				name : 'ShowEditMaskNotification',
				sender : self,
			});

		}).on('click', function(evt){evt.stopPropagation();});

		
	},
	editMode : function(aBool){
		if(aBool === true){
			this.$el.addClass('editMode');
			this.$el.draggable( "disable" );
			this.set('selected', false); 
		}
		else if(aBool === false){
			this.$el.draggable( "enable" );
			this.$el.removeClass('editMode');  
			this.$el.css('zIndex', 1000);
		}

		return this.$el.hasClass('editMode');
	},
	_onMouseDown : function(evt){
		
		if(!this.get('editMode')){
			this._super(evt);
		}
		else
		{ 
			if(evt.which < 2){
				evt.stopPropagation();
				evt.preventDefault();
				this.$el.focus();
				this._createBoxSelector(evt);
			}
		}
	},
	_onFocus : function(evt){
		if(!_.contains(UIBuilder.GroupDraggingManager.groupedComponents, this))
		{

			AppKit.NotificationCenter.post({
					name : 'UnselectComponentsNotification',
					sender : null
			});

			if(!this.get('editMode')){
				this.set('selected', true);
			}

			UIBuilder.GroupDraggingManager.groupedComponents = [];
		}
	},
	_createBoxSelector : function(evt1)
	{
		var offset = this.$el.offset(); 

		var startX = evt1.pageX - offset.left,
			startY = evt1.pageY - offset.top;

		var boxDiv = $('<div>').addClass('uibuilder-grouping-box')
				     .css({
				     	left : startX,
				     	top : startY,
				     	width : 0,
				     	height : 0
				     })
					 .appendTo(this.$el);

		var evtID = _.uniqueId('evt-');
		var self = this;

		UIBuilder.GroupDraggingManager.groupedComponents = [];

		$(document).on('mousemove.'+evtID, function(evt2){
			
			if(boxDiv){

				var endX = evt2.pageX - offset.left,
					endY = evt2.pageY - offset.top;

				var left = Math.min(startX, endX),
					top = Math.min(startY, endY);

				boxDiv.css({
					left : left,
					top : top,
					width : Math.abs(endX - startX),
					height : Math.abs(endY - startY)
				});

				UIBuilder.GroupDraggingManager.groupedComponents = [];
				self.component.$el.children('.uibuilder-editframe').each(function(idx, elem ){
					 var editFrame =  $(elem).widget();
					 var intersects = boxDiv.intersectsEl($(elem));
					 editFrame.set('selected', intersects);
					 if(intersects){
					 	UIBuilder.GroupDraggingManager.groupedComponents.push(editFrame); 
					 }
				});
			}

		
		}).on('mouseup.'+evtID, function(){
			$(document).off('mousemove.'+evtID + ' mouseup.'+evtID);
			boxDiv.remove();
			boxDiv = null; 
		});

	}
});


