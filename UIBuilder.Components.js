//= require ./UIBuilder.js

UIBuilder.Components = {
    prototypes : {},
    register : function(idName, config, Klass){
        UIBuilder.Components.List.push(new Backbone.Model(_.extend({id : idName}, config)));
        UIBuilder.Components.prototypes[idName] = Klass;
    },
    create : function(idName, options)
    {
        if(UIBuilder.Components.prototypes[idName]){
            return new UIBuilder.Components.prototypes[idName](options);
        }
    },
    load : function()
    {
        var count = UIBuilder.Components.List.length,
        i = 0;
    
        for(; i < count; i++){
            
            var m = UIBuilder.Components.List.at(i);

            new UIBuilder.Components.Item({
                model : m
            }).$el.appendTo($("#ComponentList"));
        }
    }
};

UIBuilder.Components.List = new Backbone.Collection();


jQuery(function(){

	UIBuilder.Components.Item = AppKit.View.extend({
			 template : $('#ComponentItemTemplate').template(), 
	         className : 'component-item',
	    	 initialize : function(options){

	         	this._super(options);
	         	var self = this;  

	         	this.$el.draggable({
	         		appendTo: "body",
	         		revert : "invalid",
      				cursorAt: this.model.get('cursorAt'),
                    start : function(evt, ui)
                    {
                        AppKit.NotificationCenter.post({
                            name : 'EndEditMaskNotification' 
                        });

                        UIBuilder.DDManager.initDrag(ui.helper);
                    },
                    stop : function(evt, ui){
                        UIBuilder.DDManager.endDrag();
                    },
                    drag : function(evt, ui){
                        UIBuilder.DDManager.highlightDropView();
                    },
	         		helper: function(){
	         			if(self.model.get('id')){
                            var helper = UIBuilder.Components.create(
                                self.model.get('id')).$el;

                            if(helper.droppable('instance')){
                                helper.droppable('destroy');
                            }

	         				return helper.get(0);
	         			}
	         			return self.$el.clone().get(0);
	         		} 
	         	});

	         	this.$el.data('componentID', this.model.get('id'));
	         } 
	}); 

});


AppKit.launch(function(){
    UIBuilder.Components.load();
});
