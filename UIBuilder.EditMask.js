UIBuilder.EditMask = AppKit.View.extend({
	editFrame : null, 
	className : 'uibuilder-edit-mask',
	initialize : function(options){
 
		if(this.editFrame){
			this.editFrame.set('editMask', this);
		}
	},
	show : function()
	{
		if(this.editFrame){

			this.editFrame.set('editMask', this);

			var p = this.editFrame.$el.offset(),
				left = new Number(p.left).valueOf(),
				top = new Number(p.top).valueOf();

			if(this.editFrame instanceof UIBuilder.EditViewFrame){
				
				this._activeEditor = this.editFrame;
				this._activeEditor.parent = this.editFrame.$el.parent();
 				this._activeEditor.parent.parents('.uibuilder-editframe').moveToTop();
				 
				var hAdj = (this.$el.parent().outerHeight() - this.$el.parent().innerHeight())/2.0;
		
				this._activeEditor.$el.appendTo(this.$el).css({
						left : left - this.$el.offset().left,
						top : top - this.$el.parent().offset().top - hAdj
				});

				
				this._activeEditor.set('editMode', true);

			}
			else
			{
				this._activeEditor = this.editFrame.editor();
 
				if(this._activeEditor){

					var hAdj = (this.$el.parent().outerHeight() - this.$el.parent().innerHeight())/2.0;

					this._activeEditor.appendTo(this.$el).css({
						left : left - this.$el.offset().left-3,
						top : top - this.$el.parent().offset().top-3 - hAdj
					});

					var input = this._activeEditor.children('textarea');
					if(_.isFunction(this.editFrame.component.edit)){
						input.empty();
						input.html(this.editFrame.component.edit());
					}
					 
					this.editFrame.set('selected', false);
					setTimeout(function(){
						input.select(); 
					}, 0);
				}
			}

			this.$el.show();
		}
	},
	remove : function()
	{
		if(this._activeEditor){
			if(this._activeEditor instanceof UIBuilder.EditViewFrame){
				 
				var wAdj = (this._activeEditor.parent.outerWidth() - this._activeEditor.parent.innerWidth())/2.0,
		 			hAdj = (this._activeEditor.parent.outerHeight() - this._activeEditor.parent.innerHeight())/2.0;
		
				var p = this._activeEditor.$el.offset(),
					left = new Number(p.left).valueOf(),
					top = new Number(p.top).valueOf();

				this._activeEditor.$el.css({
					left : left -  this._activeEditor.parent.offset().left - wAdj,
					top : top - this._activeEditor.parent.offset().top - hAdj
				}).appendTo(this._activeEditor.parent); 

				this._activeEditor.set('editMode', false);
				this._activeEditor.unset('parent');
				//this._activeEditor.$el.droppableFix();
			}
			 
		}

		this.$el.remove();  

		if(this.editFrame){
			this.editFrame.unset('editMask');
		} 
	}
});