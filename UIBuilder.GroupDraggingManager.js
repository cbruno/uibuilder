



UIBuilder.GroupDraggingManager = {
	groupedComponents : [],
	_startingPositions : {},
	move : function(delta){
		var count = this.groupedComponents.length,
			i = 0;
		//drag all the grouped components
	 	for(; i < count; i++){
	 		var component = UIBuilder.GroupDraggingManager.groupedComponents[i];
	 		
	 		var pos = component.$el.position();
 			component.$el.css({
	 			left : pos.left + delta.x,
	 			top : pos.top + delta.y
 			});

 			component.set('selected', false);
	 		
	 	}
	},
	startDrag : function(skip){
		var count = this.groupedComponents.length,
			i = 0; 
		var maxZ = 0; 
	 	for(; i < count; i++){
	 		var component = UIBuilder.GroupDraggingManager.groupedComponents[i];
	 		this._startingPositions[component.id] = component.$el.position(); 
	 		if(i == 0){
	 			maxZ = component.$el.parent().maxZ();
	 		}

	 		component.$el.css('zIndex', maxZ);
	 	}
	},
	revertDrag : function(skip)
	{
		var count = this.groupedComponents.length,
			i = 0; 
	 	for(; i < count; i++){
	 		var component = UIBuilder.GroupDraggingManager.groupedComponents[i];
	 		var p = this._startingPositions[component.id];
	 		component.$el.animate({
	 			left : p.left,
	 			top : p.top
	 		});
	 	}

	 	this.groupedComponents = [];
	},
	completeDrop : function(aView)
	{
		var count = this.groupedComponents.length,
			i = 0; 

		 var wAdj = (aView.$el.outerWidth() - aView.$el.innerWidth())/2.0,
			 hAdj = (aView.$el.outerHeight() - aView.$el.innerHeight())/2.0;
		
	 	for(; i < count; i++){
	 		var component = UIBuilder.GroupDraggingManager.groupedComponents[i];
	 		var cOffset = component.$el.offset(),
	 			vOffset = aView.$el.offset();
	 		
	 		component.$el.css({
	 			left : cOffset.left - vOffset.left - wAdj,
				top : cOffset.top - vOffset.top - hAdj 
	 		});

	 		component.$el.appendTo(aView.$el);
	 		component.set('selected', true);	
	 	}


	}
};