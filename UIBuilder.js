UIBuilder = {};

(function ($) {

	$.fn.containsEl = function(elem){
		var eOffset = elem.offset(),
			thisOffset = $(this).offset();

		return ( eOffset.left > thisOffset.left 
				 && eOffset.left + elem.width() < thisOffset.left + $(this).width()
				 && eOffset.top > thisOffset.top
				 && eOffset.top + elem.height() < thisOffset.top + $(this).height());
	};

 

	$.fn.intersectsEl = function(elem){
		var eOffset = elem.offset(),
			thisOffset = $(this).offset(); 

		return !(eOffset.left > thisOffset.left + $(this).width() ||
				eOffset.top > thisOffset.top + $(this).height() ||
				thisOffset.left > eOffset.left + elem.width() ||
				thisOffset.top > eOffset.top + elem.height() );
	};

	$.fn.maxZ = function(){
		var topZ = 0;
		$(this).children().each(function(){
			  var thisZ = parseInt($(this).css('zIndex'), 10);
			  if (thisZ > topZ){
			     topZ = thisZ;
			  } 
		});

		return topZ; 
	};

	$.fn.moveToTop = function()
	{
		$(this).css('zIndex', $(this).parent().maxZ());
	};


})(jQuery)