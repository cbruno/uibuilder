//= require ./button.js
//= require ./watermark.js

//editable textfield with drop down menu


$.widget("ui.combobox", {

    options: {
        radio: false,
        placeholder: null,
        enabled: true
    },
    _create: function () {

        this.element.addClass("ui-combobox")
            .attr("role", "combobox").
            data('ui.combobox', true);

        this._selectedValue = null;
        this._selected = -1;

        this._createInput();
        this._createMenu();
        this._createMenuButton();
    },

    _createInput: function () {

        this.input = $("<input type='text'>")
            .appendTo(this.element)
            .attr("title", "")
            .addClass("ui-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left");

        var self = this;
        this.input.on('keydown', function (evt) {

            if (!self.options.enabled)
                return;

            if (evt.keyCode === $.ui.keyCode.DOWN) {
                evt.preventDefault();
				evt.stopPropagation();
				
				if (self._menu.is(":visible")) {
                    self._menu.focus();
                    self._menu.menu("highlight", 0);
                }
                else {
                    self._menu.outerWidth(self.element.width());
                    var offset = self.element.offset();
					self._menu.css({
                        left: offset.left,
                        top: offset.top + self.element.height() - 2
                    });

					var ymax = offset.top + self.element.height() -2  + self._menu.height();
					if(ymax >= $(window).height()){
						self._menu.css({
							top : offset.top - self._menu.outerHeight() - 3
						});
					}

                    self._menu.menu("open");
                }
            }
			else if(evt.keyCode === $.ui.keyCode.UP)
			{
				evt.preventDefault();
				evt.stopPropagation();
				
				if (self._menu.is(":visible")) {
                    self._menu.focus();
                    self._menu.menu("highlight", self._menu.children('.ui-menu-item').length-1);
                }
                else {
                    self._menu.width(self.element.width());
                    var offset = self.element.offset();
					self._menu.css({
                        left: offset.left,
                        top: offset.top + self.element.height() - 2
                    });

					var ymax = offset.top + self.element.height() -2  + self._menu.height();
					if(ymax >= $(window).height()){
						self._menu.css({
							top : offset.top - self._menu.outerHeight() - 3
						});
					}

                    self._menu.menu("open");
                }
			}
        });
    },
    setMenu : function(aMenu){
        
        if(this._menu){
            this._menu.remove();
        }

        this._menu = null;

        if(aMenu){
            this._menu = aMenu; 
            this._menu.addClass("ui-combobox-menu");
            var self = this;
            this._menu.on("selection", function (event, menuItem) {
                self.selected($(this).children('.ui-menu-item').index(menuItem.element));
            });
        }
         
    },
    _createMenu: function () {
        
        var m = this.element.children(".appkit-menu");
        if (m.length == 0) {
            m = this.element.data("menu");
        }
        else {
            m = m.first();
        }
        if (m) {
           
            this.setMenu(m);
        }
    },
    selected: function (idx) {
        if (idx !== undefined) {
            var menuItems = this._menu.children(".ui-menu-item");
            if (idx > -1 && idx < menuItems.length) {
                var menuItem = menuItems.get(idx);
                if (menuItem) {
                    $(menuItems).menuitem("checked", false);
                    if (this.options.radio) {
                        $(menuItem).menuitem("checked", true);
                    }
                    this._selected = idx;
                    this._selectedValue = $(menuItem).data("value");
                    if(this._selectedValue === undefined){
                        this._selectedValue = $(menuItem).text();
                    }
                    this.element.trigger("selection", [this._selected, this._selectedValue ]);
                }

            }
        }

        return this._selected;
    },
    selectedValue: function (aString) {
        if (aString !== undefined) {
            var menuItems = this._menu.children(".ui-menu-item"),
                count = menuItems.length,
                i = 0;
            for (; i < count; i++) {
                var menuItem = menuItems.get(i);
                if ($(menuItem).data('value') === aString) {
                    this.selected(i);
                    break;
                }
            }
        }

        return this._selectedValue;
    },
    _createMenuButton: function () {

        var self = this,
            wasOpen = false;

        $("<div>")
            .html("<div class='icon-chevron-down'></div>")
            .attr("tabIndex", -1)
            .attr("title", "Show Menu")
            .appendTo(this.element)
            .button({
                text: false
            })
            .removeClass("ui-corner-all")
            .addClass("ui-combobox-toggle")
            .mousedown(function (evt) {
                if(self._menu){
                    wasOpen = self._menu.is(":visible");

                    self._menu.width(self.element.width());
                    var offset = self.element.offset();
                    
                    self._menu.css({
                        left: offset.left,
                        top: offset.top + self.element.height() - 2
                    });

					var ymax = offset.top + self.element.height() -2  + self._menu.height();
					if(ymax >= $(window).height()){
						self._menu.css({
							top : offset.top - self._menu.outerHeight() - 3
						});
					}
                }
                
            })
            .click(function () {

                if (!self.options.enabled)
                    return;

                self.input.focus();
                if(self._menu){
                    if (wasOpen)
                        self._menu.menu("close");
                    else
                        self._menu.menu("open");
                }
                
            });
    },
    _setOption: function (key, value) {
        if (key === "placeholder") {
            if (value) {
                this.input.watermark(value);
            }
            else
                $.watermark.hide(this.input);
        }

        if (key === "enabled") {
            if (value) {
                this.element.removeClass("disabled");
                this.input.removeClass("disabled");
                this.input.removeAttr("readonly");
                this.input.attr('tabindex', 0);
            }
            else {
                this.element.addClass("disabled");
                this.input.addClass("disabled");
                this.input.attr("readonly", "true");
                this.input.attr('tabindex', -1);
            }
        }

        this._super(key, value);
    },
    value: function (aValue) {
        if (aValue !== undefined)
            this.input.val(aValue);

        return this.input.val();
    }

});