$.widget('ui.splitview', {

    options: {
        dividerThickness: 10,
        fixed: false,
        orientation: 'horizontal'
    },
    _create: function () {
        this.element.addClass('ui-splitview').data('ui.splitview', true);

        this.element.children().addClass('view');

        this._createSplitter();


    },
    splitterPosition: function (pos) {
        if (pos === undefined) { //getter
            if (this.options.orientation === "vertical") {
                return Math.floor(this._divider.position().top);
            }
            else {
                return Math.floor(this._divider.position().left);
            }
        }
        else { //setter

            var subviews = this.element.find('.view'),
                minPos = 0,
                maxPos = 0;

            if (this.options.orientation === "vertical") {

                var minPos = 0,
                    maxPos = this.element.height() - this.options.dividerThickness - 1;
                if (subviews.length) {
                    var minH = parseInt(subviews.eq(0).css('min-height'), 10),
                        maxH = parseInt(subviews.eq(0).css('max-height'), 10);

                    minPos = isNaN(minH) ? minPos : Math.max(minPos, minH);
                    maxPos = isNaN(maxH) ? maxPos : Math.min(maxPos, maxH);

                }

                this._divider.css("top", (Math.max(minPos, Math.min(pos, maxPos)) / this.element.height()) * 100 + '%');
            }
            else {

                var minPos = 0,
                    maxPos = this.element.width() - this.options.dividerThickness - 1

                if (subviews.length) {
                    var minW = parseInt(subviews.eq(0).css('min-width'), 10),
                        maxW = parseInt(subviews.eq(0).css('max-width'), 10);

                    minPos = isNaN(minW) ? minPos : Math.max(minPos, minW);
                    maxPos = isNaN(maxW) ? maxPos : Math.min(maxPos, maxW);

                }

                this._divider.css("left", (Math.max(minPos, Math.min(pos, maxPos)) / this.element.width()) * 100 + '%');
            }

            this.sizer();
        }
    },
    _destroy: function () {
        var e = this.element.find(".view");

        if (this._divider) {
            this._divider.remove();
        }

        e.removeClass('view');
    },
    _createSplitter: function () {

        if (this._divider) {
            this._divider.remove();
        }

        this._divider = $('<div>')
            .addClass("view-divider")
            .attr("role", "separator")
            .appendTo(this.element);


        this._divider.data("active", false);

        this._divider.addClass(this.options.orientation);

        var subviews = this.element.children('.view');

        if (subviews.length) {
            var w = subviews.eq(0).data('width') ? subviews.eq(0).data('width') : 0;
            if (subviews.eq(1).data('width')) {
                w = Math.max(w, this.element.width() - subviews.eq(1).data('width'));
            }

            var h = subviews.eq(0).data('height') ? subviews.eq(0).data('height') : 0;
            if (subviews.eq(1).data('height')) {
                h = Math.max(h, this.element.height() - subviews.eq(1).data('height'));
            }

            if (this.options.orientation === 'horizontal') {
                this._divider.width(this.options.dividerThickness);
                this.splitterPosition(w);
                subviews.height('100%');
            }
            else {
                this._divider.height(this.options.dividerThickness);
                this.splitterPosition(h);
                subviews.width('100%');
            }


        }

        this._bindEvents();

    },
    _bindEvents: function () {
        this._eventID = _.uniqueId('ui-splitview-event-');
        var self = this;
        if (this._divider) {
            this._divider.on('mousedown', function (evt) {
                evt.preventDefault();
                if (!self.options.fixed && evt.which < 2) {

                    $(this).data("active", true);
                    if (self.options.orientation === "vertical") {
                        $("body").css("cursor", "row-resize");
                    }
                    else {
                        $("body").css("cursor", "col-resize");
                    }


                    $(document).on('mousemove.' + self._eventID, function (evt2) {
                        if (self._divider.data("active")) {

                            if (self.options.orientation == "vertical") {
                                var y = Math.min(self.element.height() - self.options.dividerThickness,
                                    Math.max(0, evt2.clientY - self.element.offset().top));
                                self.splitterPosition(y);
                            }
                            else {
                                var x = Math.min(self.element.width() - self.options.dividerThickness,
                                    Math.max(0, evt2.clientX - self.element.offset().left));
                                self.splitterPosition(x);
                            }
                        }
                    }).on('mouseup.' + self._eventID, function (evt3) {
                        self._divider.data("active", false);
                        $("body").css("cursor", "inherit");

                        $(document).off('mousemove.' + self._eventID + ", mouseup." + self._eventID);
                    });

                }
            });
        }
    },
    sizer: function () {
        var subviews = this.element.children('.view');

        if (subviews.length) {
            var firstView = subviews.eq(0),
                secondView = subviews.eq(1);

            if (this.options.orientation === "vertical") {

                var h = Math.max(0, this.splitterPosition());
                firstView.css({
                    height: h + 1,
                    top: 0,
                    left: 0,
                    right: 0
                });

                secondView.css({
                    height: this.element.height() - h - this.options.dividerThickness,
                    top: h + this.options.dividerThickness,
                    left: 0,
                    right: 0
                });

            }
            else {

                var w = Math.max(0, this.splitterPosition());

                firstView.css({
                    bottom: 0,
                    top: 0,
                    left: 0,
                    width: w + 1
                });

                secondView.css({
                    bottom: 0,
                    top: 0,
                    left: w + this.options.dividerThickness,
                    width: this.element.width() - w - this.options.dividerThickness
                });

                if (firstView.data('ui.splitview')) {
                    firstView.splitview('sizer');
                }

                if (secondView.data('ui.splitview')) {
                    secondView.splitview('sizer');
                }
            }

            this.element.trigger('sizer');
        }
    }
});

$(window).resize(function () {
    $('body').find('.ui-splitview').splitview('sizer');
});