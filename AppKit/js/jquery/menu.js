jQuery.fn.isMouseOver = function () {
    return $(this).parent().find($(this).selector + ":hover").length > 0;
};


$.widget("ui.menuitem", {

    _create: function () {
        this.element.addClass("ui-menu-item");
        this.element.attr("role", "menuitem");

        this._check = $("<div>").addClass("ui-menuitem-check icon-check");
        this.element.append(this._check);

        this.checked((this.state === "on"));

        var submenu = this.element.children(".appkit-menu");
        if (submenu.length > 0) {
            submenu.addClass("submenu");
            submenu.data("supermenu", this.element.parent());
            this.element.data("submenu", submenu);
            var submenuIndicator = $("<div class='icon-play'></div>").addClass("ui-menuitem-accessory");
            this.element.append(submenuIndicator);
        }

        var self = this;
        this.element.bind({
            mouseover: function (evt) {
                if (self.element.hasClass("disabled"))
                    return;

                evt.stopPropagation();

                self.open();
            },
            mouseout: function (evt) {
                if (!$(this).data("submenu"))
                    $(this).removeClass("highlighted");
            },
            click: function (evt) {
                self.fire();
            }
        });

    },
    close: function () {
        var submenu = this.element.data("submenu");
        this.element.removeClass("highlighted");
        if (submenu && submenu.length > 0) {
            if (!submenu.isMouseOver()) {
                submenu.hide();
                this.element.removeClass("highlighted");
            }
            else
                this.element.addClass("highlighted");
        }
    },
    open: function () {
        this.element.parent().find(".submenu").hide();
        this.element.parent().children(".appkit-menu-item").removeClass("highlighted");
        this.element.addClass("highlighted");
        this.element.data("menu").focus();

        var submenu = this.element.data("submenu");

        if (submenu && submenu.length > 0) {

            var over = this.element.offset().top + submenu.height() - $(window).height();

            //show submenu
            if (this.element.parent().offset().left + this.element.parent().width() + submenu.width() < $(window).width()) {
                submenu.css({
                    left: Math.max(0, this.element.parent().width()),
                    top: -7 - Math.max(0, over)
                });
            }
            else {
                submenu.css({
                    left: 0,
                    top: -7 - Math.max(0, over)
                });
            }

            submenu.children(".submenu").hide();
            submenu.children(".appkit-menu-item").removeClass("highlighted");
            submenu.menu("open");
        }
    },
    fire: function () {
        var submenu = this.element.data("submenu");
        if (!submenu) {
            var self = this;
            self.count = 0;
            self.element.removeClass("nohover");
            self.interval = setInterval(function () {

                if (self.element.hasClass("nohover"))
                    self.element.removeClass("nohover");
                else {
                    self.element.addClass("nohover");
                    self.count++;
                }

                if (self.count > 1) {
                    clearInterval(self.interval);
                    self.element.removeClass("nohover");

                    self.element.parent().menu("selected", self);
                    self.element.parent().trigger("__doclose__");

                    setTimeout(function () {
                        self.element.trigger("fire");
                    }, 100);
                }
            }, 50);
        }

    },
    enabled: function (aBOOL) {
        if (aBOOL == undefined)
            return !this.element.hasClass("disabled");

        if (aBOOL) {
            this.element.removeClass("disabled");
        }
        else {
            this.element.addClass("disabled");
        }

    },
    highlight: function (aBool) {
        if (aBool == undefined)
            return this.element.hasClass("highlighted");

        if (aBool) {
            this.element.addClass("highlighted");

        }
        else {
            this.element.removeClass("highlighted");
        }
    },
    checked: function (aBool) {
        if (aBool === undefined)
            return this._check.hasClass("checked");

        if (aBool) {
            this._check.addClass("checked");
        }
        else {
            this._check.removeClass("checked");
        }
    },
    itemIndex: function () {
        return this.element.index();
    }

});


$.widget("ui.menu", {

    _create: function () {

        this.element.addClass("ui-menu");
        this.element.attr("role", "menu");
        this.element.attr("tabindex", 0);

        this.element.children(".appkit-menu-item").menuitem();

        if (this.element.parents(".appkit-menubar").length == 0 &&
            this.element.parents(".appkit-menu-item").length == 0) {
            if (this.element.parent() !== $("body")) {
                this.element.parent().data("menu", this.element);
            }
            this.element.appendTo($("body"));
        }

        this.element.children(".appkit-menu-item").data("menu", this.element);

        var self = this;
        this.element.bind({
            mousedown: function (evt) {
                evt.stopPropagation();
            },
            mouseup: function (evt) {
                evt.stopPropagation();
                evt.preventDefault();
            },
            mouseenter: function (evt) {
                evt.stopPropagation();
            },
            keydown: function (evt) {
                
				evt.stopPropagation();
                evt.preventDefault();
                
				if (evt.which == $.ui.keyCode.ESCAPE) //esc
                {
                    self.close();
                }
                else if (evt.which == $.ui.keyCode.SPACE ||
                    evt.which == $.ui.keyCode.ENTER) //enter
                {
                    var highlighted = $(this).children(".appkit-menu-item.highlighted");
                    if (highlighted) {
                        highlighted.menuitem("fire");
                    }
                }
                else if (evt.which == $.ui.keyCode.LEFT) //left
                {
                    var supermenu = $(this).data("supermenu");
                    if (supermenu) {
                        $(this).hide();
                        $(this).children(".appkit-menu-item").removeClass("highlighted");
                        supermenu.trigger("keydown", [37]);
                        supermenu.focus();
                    }

                }
                else if (evt.which == $.ui.keyCode.RIGHT) //right arrow
                {
                    var highlighted = $(this).children(".appkit-menu-item.highlighted");
                    if (highlighted) {
                        var submenu = highlighted.data("submenu");
                        if (submenu) {
                            var menuitems = submenu.children(".appkit-menu-item");

                            if (menuitems) {
                                var index = 0;
                                var nextItem = menuitems.eq(index)
                                while (index < menuitems.length && !nextItem.menuitem("enabled")) {
                                    index++;
                                    nextItem = $(menuitems.get(index));
                                }


                                if (nextItem.menuitem("enabled")) {
                                    nextItem.menuitem("open");
                                }
                            }

                            submenu.focus();
                        }
                        else {
                            var supermenu = $(this).data("supermenu");
                            while (supermenu.data("supermenu")) {
                                supermenu = supermenu.data("supermenu");
                            }

                            if (supermenu) {
                                supermenu.trigger("keydown", [39]);
                                supermenu.focus();
                            }

                        }
                    }
                }
                else if (evt.which == $.ui.keyCode.DOWN) //down arrow
                {
                    var highlighted = $(this).children(".appkit-menu-item.highlighted");

                    if (highlighted.length === 0) //nothing highlighed, get the first
                    {
                        var menuitems = $(this).children(".appkit-menu-item");
                        var nextItem = menuitems.first();
                        while (0 < menuitems.length && !nextItem.menuitem("enabled")) {
                            nextItem = menuitems.first();

                        }

                        if (nextItem.menuitem("enabled")) {
                            nextItem.menuitem("open");
                        }
                        nextItem.get(0).scrollIntoView(false);

                    }


                    if (highlighted.length > 0) {
                        var menuitems = $(this).children(".appkit-menu-item");
                        var index = menuitems.index(highlighted);
                        if (index + 1 < menuitems.length) {
                            index++;
                            var nextItem = menuitems.eq(index);
                            while (index + 1 < menuitems.length && !nextItem.menuitem("enabled")) {
                                index++;
                                nextItem = menuitems.eq(index);

                            }

                            if (nextItem.menuitem("enabled")) {
                                highlighted.menuitem('close');
                                nextItem.menuitem("open");
                            }
                            nextItem.get(0).scrollIntoView(false);
                        }
                    }

                }
                else if (evt.which == $.ui.keyCode.UP) //up arrow
                {
                    var highlighted = $(this).children(".appkit-menu-item.highlighted");

                    if (highlighted.length == 0) {
                        if (highlighted.length == 0) //nothing highlighed, get the first
                        {
                            var menuitems = $(this).children(".appkit-menu-item");
                            var nextItem = menuitems.last();
                            while (!nextItem.menuitem("enabled")) {
                                nextItem = menuitems.last();

                            }

                            if (nextItem.menuitem("enabled")) {
                                nextItem.menuitem("open");
                            }
                            nextItem.get(0).scrollIntoView(true);
                        }
                    }


                    if (highlighted.length > 0) {
                        var menuitems = $(this).children(".appkit-menu-item");
                        var index = menuitems.index(highlighted);
                        if (index - 1 > -1) {
                            index--;
                            var nextItem = menuitems.eq(index);
                            while (index - 1 > -1 && !nextItem.menuitem("enabled")) {
                                index--;
                                nextItem = menuitems.eq(index);
                            }

                            if (nextItem.menuitem("enabled")) {
                                highlighted.menuitem("close");
                                nextItem.menuitem("open");
                            }
                            nextItem.get(0).scrollIntoView(true);

                        }
                        else if (index < 0) {
                            menuitems.last().menuitem("open");
                        }


                    }


                }
            }
        });


        this.element.bind('__doclose__', function () {
            self.close();
        });
    },
    open: function () {

        var self = this;
        $(".appkit-menu").filter(":visible").filter(function () {
            return !$.contains(this, self.element.get(0));
        }).trigger("__doclose__");

        this.element.find(".appkit-menu-item").removeClass("highlighted");
        this.element.find(".submenu").hide();

        this.element.show();


    },
    close: function () {

        this.element.hide();

        var submenus = this.element.children(".submenu");
        if (submenus && submenus.length > 0 && !submenus.is(":hover"))
            submenus.hide();

        if (this.element.data("supermenu")) {
            var supermenu = this.element.data("supermenu");
            supermenu.menu("close");
        }

        this.element.trigger('close');

    },
    selected: function (menuItem) {
        if (menuItem === undefined)
            return this._selected;

        this._selected = menuItem;

        this.element.trigger("selection", this._selected);
    },
    highlight: function (anIndex) {
        var menuitems = this.element.children(".appkit-menu-item");

        if (menuitems.length > anIndex && anIndex > -1)
            $(menuitems.get(anIndex)).menuitem("open");
    }
});

jQuery(function () {

    $(document).mousedown(function () {
        $('.appkit-menu').filter(":visible").trigger('__doclose__');
    });

	$(window).resize(function () {
        $('.appkit-menu').filter(":visible").trigger('__doclose__');
    });

    $(document).keydown(function (evt) {

        if (evt.which == $.ui.keyCode.ESCAPE) //esc
        {
            $('.appkit-menu').filter(":visible").trigger('__doclose__');
        }
    });
});
