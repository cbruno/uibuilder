//= require ../jquery/jquery.js
//= require ./underscore.js
//= require ./backbone.js

//Use mustache style templating
_.templateSettings = {
    evaluate: /\{\[([\s\S]+?)\]\}/g,
    interpolate: /\{\{([\s\S]+?)\}\}/g
};

/* set/get value on objects, allows for computed properties */
_.mixin({
    //gets the value of an attribute in a javascript object
    getKeyValue: function (anObject, key) {
        if (_.isFunction(anObject[key])) {
            return anObject[key].call(anObject);
        }
        else {
            return anObject[key];
        }
    },
    //sets the value of an attribute in a javascript object
    setKeyValue: function (anObject, key, aValue) {

        if (_.isFunction(anObject[key])) {
            return anObject[key].call(anObject, aValue);
        }
        else {
            anObject[key] = aValue;
			return aValue;
        }
    },
    setKeyPath: function (anObject, aPath, aValue) {

        var paths = aPath.trim().split('.'),
            count = paths.length,
            obj = anObject,
            i = 0;
        for (; i < count; i++) {
            obj = obj[paths[i]];
            if (obj === undefined)
                break;
            if (i === count - 2) {
                obj[paths[i + 1]] = aValue;
                return aValue;
            }
        }
    },
    getKeyPath: function (anObject, aPath) {

        var paths = aPath.trim().split('.'),
            count = paths.length,
            obj = anObject,
            i = 0;

        for (; i < count; i++) {
            obj = obj[paths[i]];
            if (obj === undefined)
                break;
        }

        return obj;
    }
});


//setup AppKit
var AppKit = {
    _classRegistry: {},
    _executables: [],
    get: function (objectID) {

        if (!objectID)
            return undefined;

        if (_.isObject(objectID))
            return objectID;

		var sel = $('*[data-id="'+ objectID+'"]');
		if(sel.length){
			return sel.widget();
		}

        //otherwise get global namespaced object
        return _.getKeyPath(window, objectID);
    },
    launch: function (aFunction) {
        if (_.isFunction(aFunction)) {
            AppKit._executables.push(aFunction);
        }
    }
};


(function ($) {

	// Store a reference to the original remove method.
	var _origRemove = jQuery.fn.remove;
	
	jQuery.fn.remove = function () {
			
			$(this).each(function(idx, el){
					
				var widget = $(el).widget();
				if(widget){
					if(_.isFunction(widget.destroy)){
						widget.destroy.call(widget);
					}
				}
			});
	        return _origRemove.apply(this, arguments);
	};

    $.fn.awaken = function (classLst) {

        var objects = [],
            paths = [];

        var _wake = function (item, el) {
            if (!el) {
                return;
            }

            if (el.parents('.AppKit-template').length) //do not need to awaken templates
            {
                return;
            }

            var Class = null;
            try {
                Class = AppKit._classRegistry[item].Class;
            } catch (e) {
                console.error(item + ' not a valid class');
                return;
            }

            var aData = _.extend({}, el.data());
            //if no id, take it of the DOM element
            if (!aData.id) {
                aData.id = el.attr('id');
            }
            //no id, make one
            if (!aData.id) {
                aData.id = _.uniqueId(item + '-');
            }

            var theInstance = new Class(_.extend(aData, {el: el}));
            objects.push(theInstance);

            if (aData.outlet) {
                var outlets = aData.outlet.trim().split(/\s+/),
                    count = outlets.length,
                    k = 0;
                for (; k < count; k++) {
                    paths.push({outlet: outlets[k], object: theInstance});
                }
            }

        };

        var classes = _.keys(AppKit._classRegistry);
        if (classLst) {
            classes = classLst.trim().split(/\s+/);
        }

        var count = classes.length,
            i = 0;

        for (; i < count; i++) {
            var item = classes[i];

            if (this.hasClass(item)) {
                _wake(item, this);

            }

            var subEl = this.find("." + item),
                l = subEl.length,
                j = 0;

            for (; j < l; j++) {
                _wake(item, subEl.eq(j));

            }
        }

        count = paths.length,
            i = 0;
        for (; i < count; i++) {
            _.setKeyPath(window, paths[i].outlet, paths[i].object);
        }

        count = objects.length,
            i = 0;

        for (; i < count; i++) {
            if (_.isFunction(objects[i].onAwake)) {
                objects[i].onAwake.call(objects[i]);
            }
        }

        return $(this);
    };

 	$.fn.widget = function () {
        return $(this).data('widget');
    };

    //* similar to hasClass, has attribute plugin for jQuery */
    $.fn.hasAttr = function (aString) {
        return this.attr(aString) !== undefined && this.attr(aString) !== false;
    };

    $.fn.template = function () {
        return _.template(this.html());
    };


})(jQuery);

//application start
$(function () {
    $(window).load(function () {
        $('body').awaken();
        $('body').css('visibility', 'visible');
        var count = AppKit._executables.length,
            i = 0;
        for (; i < count; i++) {
            if (_.isFunction(AppKit._executables[i])) {
                AppKit._executables[i].call();
            }
        }
    });
});