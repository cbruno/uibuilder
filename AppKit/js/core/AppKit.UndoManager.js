//= require ./AppKit.RunLoop.js

AppKit.Invocation = function (options) {
    options || (options = {});
    _.extend(this, options);
};

_.extend(AppKit.Invocation.prototype, {
    target: null,
    action: null,
    argument: null,
    invoke: function () {
        if (this.target) {
            if (this.action && _.isFunction(this.target[this.action])) {
                this.target[this.action].apply(this.target, [this.argument]);
            }
        }
    }
});


AppKit._UndoGrouping = function (anUndoGrouping) {
    this._invocations = [];
    this._parent = anUndoGrouping;
};

_.extend(AppKit._UndoGrouping.prototype, {
    actionName: '',
    parent: function () {
        return this._parent;
    },
    invocations: function () {
        return this._invocations;
    },
    addInvocation: function (anInvocation) {
        this._invocations.push(anInvocation);

    },
    addInvocationsFromArray: function (anArray) {
        var count = anArray.length,
            i = 0;

        for (; i < count; i++) {
            this._invocations.push(anArray[i]);
        }
    },
    removeInvocationWithTarget: function (aTarget) {
        var index = this._invocations.length;
        while (index--) {
            if (this._invocations[index].target === aTarget) {
                this._invocations.splice(index, 1);
            }
        }
    },
    invoke: function () {
        var index = this._invocations.length;
        while (index--) {
            this._invocations[index].invoke();
        }
    }
});

AppKit._UndoGroupingPool = [];

AppKit._UndoGrouping._poolUndoGrouping = function (anUndoGrouping) {
    if (!anUndoGrouping || AppKit._UndoGroupingPool.length >= 5)
        return;

    AppKit._UndoGroupingPool.push(anUndoGrouping);
};

AppKit.UndoManager = {
    _state: 'normal', //can be "normal", "undoing", or "redoing"
    _currentGrouping: null,
    _redoStack: [],
    _undoStack: [],
    beginUndoGrouping: function () {
        this._beginUndoGrouping();
        /* when a UI event occurs, stop grouping undos */
        if (!this._registeredWithRunLoop) {
            this._registeredWithRunLoop = true;
            var self = this;
            AppKit.RunLoop.addAction({
                id: "undoManager",
                action: function () {
                    self.endUndoGrouping();
                    self._registeredWithRunLoop = false;
                }
            });
        }
    },
    canUndo: function () {
        return this._undoStack.length > 0;
    },
    canRedo: function () {
        return this._redoStack.length > 0;
    },
    endUndoGrouping: function () {
        if (this._currentGrouping) {
            var parent = this._currentGrouping.parent();
            if (!parent && this._currentGrouping.invocations().length > 0) {
                var stack = (this._state === "undoing") ? this._redoStack : this._undoStack;
                stack.push(this._currentGrouping);

            }
            else {
                parent.addInvocationsFromArray(this._currentGrouping.invocations());
                AppKit._UndoGrouping._poolUndoGrouping(this._currentGrouping);
            }

            this._currentGrouping = parent;
        }
    },
    groupingLevel: function () {

        if (!this._currentGrouping)
            return 0;

        var grouping = this._currentGrouping,
            level = this._currentGrouping ? 1 : 0;

        while (grouping = grouping.parent())
            ++level;

        return level;
    },
    redo: function () {
        if (this._redoStack.length <= 0)
            return;

        var oldUndoGrouping = this._currentGrouping,
            undoGrouping = this._redoStack.pop(),
            actionName = undoGrouping.actionName;

        this._currentGrouping = null;
        this._state = 'redoing';

        this._beginUndoGrouping();
        undoGrouping.invoke();
        this.endUndoGrouping();

        AppKit._UndoGrouping._poolUndoGrouping(undoGrouping);

        this._currentGrouping = oldUndoGrouping;
        this._state = 'normal';

        this._undoStack[this._undoStack.length - 1].actionName = actionName;

    },
    registerUndoWithTarget: function (config) //config has target, action, argument passed to action
    {
        var invocation = new AppKit.Invocation(config);
        this._addUndoInvocation(invocation);
    },
    undo: function () {
        if (this.groupingLevel() === 1)
            this.endUndoGrouping();

        this.undoNestedGroup();

    },
    undoNestedGroup: function () {
        if (this._undoStack.length <= 0)
            return;

        var undoGrouping = this._undoStack.pop(),
            actionName = undoGrouping.actionName;

        this._state = 'undoing';

        this._beginUndoGrouping();
        undoGrouping.invoke();
        this.endUndoGrouping();

        AppKit._UndoGrouping._poolUndoGrouping(undoGrouping);

        this._state = 'normal';

        this._redoStack[this._redoStack.length - 1].actionName = actionName;

    },
    _beginUndoGrouping: function () {
        this._currentGrouping = new AppKit._UndoGrouping(this._currentGrouping);
    },
    _addUndoInvocation: function (anInvocation) {

        if (!this._currentGrouping)
            this.beginUndoGrouping();

        this._currentGrouping.addInvocation(anInvocation);
        if (this._state === 'normal') {
            this._redoStack = [];
        }
    }
};