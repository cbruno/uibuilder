/**
 * AppKit Notification  = {
    name: null,
    object: null 
    }
 *
 **/

AppKit.NotificationCenter = {
    _registry: {},
    addObserver: function (config) // config should specify observer, action, name, object
    {
        if (this._registry[config.name] === undefined)
            this._registry[config.name] = [];

        this._registry[config.name].push({
            observer: config.observer,
            notification: {
                name: config.name,
                object: config.object
            },
            action: config.action
        });
    },
    removeObserver: function (config)
        //removes observer - config has observer, name (optional), object (optional)
    {
        if (!config.name) { //remove for all notifications
            for (var notificationName in this._registry) {
                this.removeObserver({
                    name: notificationName,
                    observer: config.observer
                });
            }
        }
        else if (!config.object) {
            var regs = this._registry[config.name];

            if (regs) {
                var count = regs.length,
                    index = 0;

                for (; index < count; index++) {
                    var reg = regs[index];
                    if (reg.observer === config.observer) {
                        regs.splice(index, 1);
                    }
                }
            }
        }
        else {
            var regs = this._registry[config.name];

            if (regs) {
                var count = regs.length,
                    index = 0;

                for (; index < count; index++) {
                    var reg = regs[index];
                    if (reg.observer === config.observer && reg.notification.object === config.object) {
                        regs.splice(index, 1);
                    }
                }
            }
        }
    },
    post: function (config) //name, sender, and  optional arguments
    {

        if (this._registry[config.name] !== undefined) {
            var regs = this._registry[config.name],
                count = regs.length,
                index = 0;

            for (; index < count; index++) {
                var reg = regs[index],
                    target = reg.observer;

                if (_.isFunction(reg.action) &&
                    (reg.notification.object === config.sender || !reg.notification.object)) {

                    reg.action.call(target, _.extend(reg.notification, config));
                }
                else if (_.isFunction(target[reg.action])
                    && (reg.notification.object === config.sender || !reg.notification.object)) {

                    target[reg.action].call(target, _.extend(reg.notification, config));
                }
            }
        }
    }
};
