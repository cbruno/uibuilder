/* add console object for IE8 */
if (!window.console) {
    window.console = {
        log: function () {
        },
        warn: function () {

        },
        error: function () {

        },
        info: function () {

        }
    };
}

//add in trim for IE8
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    };
}


/* String format */
if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] !== 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}