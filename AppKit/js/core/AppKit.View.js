var viewOptions = ['el', 'id', 'outlet', 'attributes', 'className', 'tagName', 'model', 'template'];

var View = AppKit.View = function (options) {

    options || (options = {});
    _.extend(this, _.pick(options, viewOptions));

    if (!this.id) {
        this.id = _.uniqueId('view');
    }

    this._ensureElement();

    if (this.id) {
        this.$el.data('id', this.id); 
    }

    //add AppKit class id
    if (this.classID) {
        this.$el.addClass(this.classID);
    }

	this.$el.data('widget', this);
    this.initialize.apply(this, arguments);

    this.delegateEvents();
    this._render(options);
};


var delegateEventSplitter = /^(\S+)\s*(.*)$/;

_.extend(AppKit.View.prototype, {
    classID: 'appkit-view',
    initialize: function (options) {
        this._render(options);
    },
    tagName: 'div',
    options: {hidden: false},
    // gets property/attribute
    get: function (aProperty) {
        return _.getKeyValue(this, aProperty);
    },
    //set property attribute (pass a JS object or one-by-one)
    set: function (aProperty, aValue) {
        if (_.isObject(aProperty)) {
            var self = this;
            _.each(_.keys(aProperty), function (key) {
                _.setKeyValue(self, key, this[key]);
            }, aProperty);
        }
        else {
            return _.setKeyValue(this, aProperty, aValue);
        }
    },
    unset: function (aProperty) {
        if (this[aProperty]) {
            delete this[aProperty];
        }
    },
    // Produces a DOM element to be assigned to your view. Exposed for
    // subclasses using an alternative DOM manipulation API.
    _createElement: function (tagName) {
        return document.createElement(tagName);
    },
    // Creates the `this.el` and `this.$el` references for this view
    _setElement: function (el) {
        this.$el = el instanceof $ ? el : $(el);
        this.el = this.$el[0];
    },
    _ensureElement: function () {

        var attrs = {};
        if (this.className) attrs['class'] = _.result(this, 'className');

        if (!this.el) {
            this._setElement(this._createElement(_.result(this, 'tagName')));

        } else {
            this._setElement(_.result(this, 'el'));
        }

        this._setAttributes(attrs);

    },
    _setAttributes: function (attributes) {
        this.$el.attr(attributes);
    },
    off: function (eventString, aFunction) {
        if (_.isFunction(aFunction)) {
            this.$el.off(eventString, _.bind(aFunction, this));
        }
        else {
            this.$el.off(eventString);
        }
        return this;
    },
    on: function (eventString, aFunction) {
        this.$el.on(eventString, _.bind(aFunction, this));
        return this;
    },
    hidden: function (aBOOL) {
        if (aBOOL === true) {
            this.$el.hide();
        }
        else if (aBOOL === false) {
            this.$el.show();
        }

        return !this.$el.is(':visible');
    },
    subview: function (aSelector) {

        if (!aSelector)
            return;

        var itemEl = this.$el.find(aSelector);
        if (itemEl.length) {
            return itemEl.widget();
        }

        return undefined;
    },
    _render: function (options) {

        if (!this._rendered) {

            if (this.template) {
                if (!this.model) {
                    this.model = new Backbone.Model({});
                }

                this.$el.children().remove();
                this.$el.html(this.template(this.model.attributes));
                this.$el.awaken();
            }
            //set options
            var opts = _.extend({}, this.options);
            if (options) {
                opts = _.extend(opts, _.omit(options, viewOptions));
            }

            this.set(opts);
            this._rendered = true;
        }
    },
    // Set callbacks, where `this.events` is a hash of
    //
    // *{"event selector": "callback"}*
    //
    //     {
    //       'mousedown .title':  'edit',
    //       'click .button':     'save',
    //       'click .open':       function(e) { ... }
    //     }
    //
    // pairs. Callbacks will be bound to the view, with `this` set properly.
    // Uses event delegation for efficiency.
    // Omitting the selector binds the event to `this.el`.
    // This only works for delegate-able events: not `focus`, `blur`, and
    // not `change`, `submit`, and `reset` in Internet Explorer.
    delegateEvents: function (events) {
        if (!(events || (events = _.result(this, 'events')))) return this;
        this.undelegateEvents();
        for (var key in events) {
            var method = events[key];
            if (!_.isFunction(method)) method = this[events[key]];
            if (!method) continue;

            var match = key.match(delegateEventSplitter);
            var eventName = match[1], selector = match[2];
            method = _.bind(method, this);
            eventName += '.delegateEvents' + this.cid;
            if (selector === '') {
                this.$el.on(eventName, method);
            } else {
                this.$el.on(eventName, selector, method);
            }
        }
        return this;
    },

    // Clears all callbacks previously bound to the view with `delegateEvents`.
    // You usually don't need to use this, but may wish to if you have multiple
    // Backbone views attached to the same DOM element.
    undelegateEvents: function () {
        this.$el.off('.delegateEvents' + this.cid);
        return this;
    }
});

AppKit._classRegistry['appkit-view'] = {Class: AppKit.View, options: AppKit.View.options};