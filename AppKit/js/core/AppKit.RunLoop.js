AppKit.RunLoop = {
    _performs: {},
    addAction: function (anAction) {
        if (anAction.id) {
            this._performs[anAction.id] = anAction;
        }
        else {
            this._performs[_.uniqueId('a')] = anAction;
        }
    },
    removeAction: function (anActionID) {
        delete this._performs[anActionID];
    },
    run: function () {
        _.each(this._performs, function (value, key, list) {
            if (value.action && _.isFunction(value.action)) {
                value.action(value.argument);
            }
        });
    }
};