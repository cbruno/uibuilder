//= require ../core/AppKit.View.js
//= require ../jquery/tabs.js

AppKit.TabView = AppKit.View.extend({
    classID: 'appkit-tabview',
    initialize: function (options) {
        this._tabTemplate = _.template('<li id="{{tabID}}" class="ui-tab"><div data-panel-id="{{panelID}}"' +
            'class="ui-tab-label">{{label}}</div><span title="Close" class="ui-tab-close" role="presentation">' +
            '<div class="icon-close"></div></span></li>');

        this.$el.append($('<ul>'));

        this.$el.tabs(options);

        var self = this;
        $.each(this.$el.children('.appkit-view'), function (idx, panel) {
            var tabName = $(panel).data('tab-name') ? $(panel).data('tab-name') : 'Tab';
            var panelID = $(panel).attr('id') ? $(panel).attr('id') : _.uniqueId('panel');
            $(this).attr('id', panelID);
            self.addTab(tabName, panelID, $(panel).data('closable'));
        });

        if (this.get('tabCount') > 0) {
            this.selectTabAtIndex(0);
        }
    },
    addTab: function (tabName, panelID, closable) {

        var tabID = _.uniqueId("tab");

        var tab = this._tabTemplate({
            label: $.trim(tabName).length ? tabName : "&nbsp;",
            panelID: panelID,
            tabID: tabID
        });

        this.$el.children('.ui-tabs-nav').append(tab);

        if (closable) {
            var self = this;
            this.$el.find('#' + tabID).children('.ui-tab-close').on('click', function (evt) {
                evt.preventDefault();
                self.removeTab(tabID);
            });

            this.$el.find('#' + tabID).on('keydown', function (evt) {
                if (evt.ctrlKey) {
                    if (evt.keyCode == 88) {
                        self.removeTab(tabID);
                    }
                }
            });
        }
        else {
            this.$el.find('#' + tabID).children('.ui-tab-close').hide();
        }

        this.$el.tabs('refresh');
    },
    selectTabAtIndex: function (anIndex) {
        this.$el.tabs('option', 'active', anIndex);
    },
    removeTab: function (tabID) {
        var tab = this.$el.find('#' + tabID);

        if (tab) {
            var panelID = tab.children('.ui-tab-label').data('panel-id');
            $('#' + panelID).remove();
            tab.remove();
            this.$el.tabs('refresh');
            this.$el.trigger('tabremoved');
        }
    },
    removeTabAtIndex: function (anIndex) {
        var tab = this.$el.children('.ui-tabs-nav').children('.ui-tab').eq(anIndex);
        if (tab) {
            var panelID = tab.children('.ui-tab-label').data('panel-id');
            $('#' + panelID).remove();
            tab.remove();
            this.$el.tabs('refresh');
            this.$el.trigger('tabremoved');
        }
    },
    removeAllTabs: function () {
        var count = this.get('tabCount'),
            i = 0;

        for (; i < count; i++) {
            this.removeTabAtIndex(i);
        }
    },
    tabLabelAtIndex: function (anIndex, aString) {
        if (anIndex > -1 && anIndex < this.get('tabCount')) {
            var tab = this.$el.children('.ui-tabs-nav').children('.ui-tab').eq(anIndex);
            if (tab) {
                var tabLabel = tab.children('.ui-tab-label');
                if (aString !== undefined) {
                    tabLabel.html(aString);
                }
                return tabLabel.text();
            }
        }
    },
    refresh: function () {
        this.$el.tabs("refresh");
    },
    tabCount: function () {
        return this.$el.children('.ui-tabs-nav').children('.ui-tab').length;
    },
    hidden: function (aBOOL) {
        this._super(aBOOL);
        this.$el.tabs('refresh');
    }
});