//= require ../core/AppKit.View.js
//= require ../core/AppKit.RunLoop.js

/**
 *
 * a Control is a View with input and
 * sends action messages to a
 * target object
 *
 * **/
AppKit.Control = AppKit.View.extend({
    options: {
        tabindex: 0,
        action: null,
        enabled: true,
        nextKey: null
    },
    initialize: function (options) {
        
        if (this.options.tabindex !== null)
            this.$el.attr('tabindex', this.options.tabindex);

        this._super(options);
    },
    delegate: function (aDelegate) {
        if (aDelegate !== undefined) {
            this._delegate = AppKit.get(aDelegate);
        }

        return this._delegate;
    },
    enabled: function (aBool) {
        if (aBool === true) {
            this.$el.removeClass('disabled');
            if (this.$el.hasAttr('tabindex'))
                this.$el.attr('tabindex', this.options.tabindex);
        }
        else if (aBool === false) {
            this.$el.addClass('disabled');
            if (this.$el.hasAttr('tabindex'))
                this.$el.attr('tabindex', -1);
        }

        return !this.$el.hasClass('disabled');
    },
    value: function () {
        return null;
    },
    numericValue: function (aNumber) {
        if (aNumber !== undefined) {
            this.set('value', parseFloat(aNumber));
        }

        return parseFloat(this.get('value'));
    },
    stringValue: function (aString) {
        if (aString !== undefined) {
            this.set('value', "" + aString);
        }

        return new String(this.get('value'));
    },
    action: function (aString) {
        if (_.isString(aString)) {

            var paths = aString.trim().split('.');
            if (paths.length === 1) {
                this._target = window;
            }
            else {
                this._target = AppKit.get(paths.slice(0, paths.length - 1).join('.'));
            }
            if (this._target) {
                this._action = this._target[paths[paths.length - 1]];
                if (!_.isFunction(this._action)) {
                    console.warn('No such action: "' + aString + ';" Assigned to control "' + this.id + '"');
                    this._action = undefined;
                    this._target = undefined;
                }
            } else {
                console.warn('No such action: "' + aString + ';" Assigned to control "' + this.id + '"');
                this._action = undefined;
                this._target = undefined;
            }
        }
        if (aString === null) {
            this._action = null;
            this._target = null;
        }

        return this._action;
    },
    fire: function () {
        if (_.isObject(this._target) && _.isFunction(this._action)) {
            this._action.call(this._target, this);
            AppKit.RunLoop.run();
        }
    },
    //next key can be used to quickly override the browser's default tab order
    nextKey: function (aString) {
        if (aString !== undefined) {

            this._nextKey = aString;
            var self = this;
            this.$el.off('keydown.nextkey');
            this.$el.on('keydown.nextkey', function (evt) {
                if (evt.keyCode == $.ui.keyCode.TAB) {
                    var control = AppKit.get(self._nextKey);
                    if (control) {
                        evt.preventDefault();
                        control.$el.focus();
                    }
                }
            });
        }
        return this._nextKey;
    }
});

