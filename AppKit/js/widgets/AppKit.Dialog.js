//= require ../core/AppKit.View.js
//= require ../jquery/dialog.js

AppKit.Dialog = AppKit.View.extend({
    classID: 'appkit-dialog',
    options: {
        delegate: null,
        visible: false,
        title: '',
        modal: false,
        resizable: true,
        draggable: true,
        height: 300,
        width: 300,
        left: 0,
        top: 0,
        minHeight: 150,
        minWidth: 150,
        maxHeight: 10000,
        maxWidth: 10000
    },
    initialize: function (options) {

        var self = this;
        this.$el.on('dialogopen', function (event, ui) {
            if (self._delegate && _.isFunction(self._delegate['onDialogOpen']))
                self._delegate['onDialogOpen'].call(self._delegate, self);
        });

        this.$el.on('dialogclose', function (event, ui) {
            if (self._delegate && _.isFunction(self._delegate['onDialogClose']))
                self._delegate['onDialogClose'].call(self._delegate, self);
        });

        this.$el.dialog();

        this._super(_.omit(options, 'visible'));

        //set the size
        this.set('width', _.extend(this.options, options).width);
        this.set('height', _.extend(this.options, options).height);

        //default visibility
        this.set('visible', this.options.visible || options ? options.visible : false);
    },
    delegate: function (aDelegate) {
        if (aDelegate !== undefined) {
            this._delegate = AppKit.get(aDelegate);
        }

        return this._delegate;
    },
    visible: function (aBool) {

        if (this.$el.dialog('instance')) {
            if (aBool === true) {
                this.$el.dialog('open');
            }
            else if (aBool === false)
                this.$el.dialog('close');
        }

        return this.$el.is(':visible');
    },
    open: function () {
        this.set('visible', true);
    },
    close: function () {
        this.set('visible', false);
    },
    title: function (aString) {
        if (this.$el.dialog('instance')) {
            if (aString) {
                this.$el.dialog('option', 'title', aString);
            }
            return this.$el.dialog('option', 'title');
        }

        return undefined;
    },
    modal: function (aBool) {
        if (this.$el.dialog('instance')) {
            if (aBool !== undefined)
                this.$el.dialog('option', 'modal', aBool);

            return this.$el.dialog('option', 'modal');
        }
        return false;
    },
    resizable: function (aBool) {
        if (this.$el.dialog('instance')) {
            if (aBool !== undefined)
                this.$el.dialog('option', 'resizable', aBool);

            return this.$el.dialog('option', 'resizable');
        }
        return false;
    },
    maximizable: function (aBool) {
        if (this.$el.dialog('instance')) {
            if (aBool !== undefined)
                this.$el.dialog('option', 'maximizable', aBool);

            return this.$el.dialog('option', 'maximizable');
        }

        return false;
    },
    minimizable: function (aBool) {
        if (this.$el.dialog('instance')) {
            if (aBool !== undefined)
                this.$el.dialog('option', 'minimizable', aBool);

            return this.$el.dialog('option', 'minimizable');
        }
        return false;
    },
    maximized: function (aBool) {
        if (this.$el.dialog('instance')) {
            if (aBool === true) {
                this.$el.dialog('maximize');
            }
            else if (aBool === false) {
                this.$el.dialog('restore');
            }
            return this.$el.dialog('isMaximized');
        }

        return false;

    },
    minimized: function (aBool) {
        if (this.$el.dialog('instance')) {
            if (aBool === true) {
                this.$el.dialog('minimize');
            }
            else if (aBool === false) {
                this.$el.dialog('restore');
            }

            return this.$el.dialog('isMinimized');
        }

        return false;
    },
    draggable: function (aBool) {
        if (this.$el.dialog('instance')) {
            if (aBool !== undefined)
                this.$el.dialog('option', 'draggable', aBool);

            return this.$el.dialog('option', 'draggable');
        }
        return false;
    },
    height: function (aNumber) {
        if (this.$el.dialog('instance')) {
            if (aNumber !== undefined) {
                this.$el.parent().height(aNumber);
                this.$el.dialog('option', 'height', aNumber);
            }

            return this.$el.parent().height();
        }

        return -1;
    },
    width: function (aNumber) {

        if (this.$el.dialog('instance')) {
            if (aNumber !== undefined) {
                this.$el.parent().width(aNumber);
                this.$el.dialog('option', 'width', aNumber);
            }

            return this.$el.parent().width();
        }
        return -1;
    },
    minHeight: function (aNumber) {
        if (this.$el.dialog('instance')) {
            if (aNumber !== undefined) {
                this.$el.dialog('option', 'minHeight', aNumber);
            }

            return this.$el.dialog('option', 'minHeight');
        }
        return -1;
    },
    minWidth: function (aNumber) {
        if (this.$el.dialog('instance')) {
            if (aNumber !== undefined) {
                this.$el.dialog('option', 'minWidth', aNumber);
            }

            return this.$el.dialog('option', 'minWidth');
        }
        return -1;
    },
    maxHeight: function (aNumber) {
        if (this.$el.dialog('instance')) {
            if (aNumber !== undefined) {
                this.$el.dialog('option', 'maxHeight', aNumber);
            }

            return this.$el.dialog('option', 'maxWidth');
        }
        return -1;
    },
    maxWidth: function (aNumber) {
        if (this.$el.dialog('instance')) {
            if (aNumber !== undefined) {
                this.$el.dialog('option', 'maxWidth', aNumber);
            }

            return this.$el.dialog('option', 'maxWidth');
        }
        return -1;
    },
    left: function (aNumber) {

        if (this.$el.parent().length) {
            if (aNumber !== undefined) {
                this.$el.parent().css('left', aNumber);
            }

            return this.$el.parent().offset().left;
        }

        return 0;

    },
    top: function (aNumber) {
        if (this.$el.parent().length) {
            if (aNumber !== undefined) {
                this.$el.parent().css('top', aNumber);
            }

            return this.$el.parent().offset().top;
        }
        return 0;
    },
    center: function () {
        if (this.$el.parent().length) {
            this.$el.parent().css({
                left: ($(window).width() - this.get('width')) / 2.0,
                top: ($(window).height() - this.get('height')) / 2.0
            });
        }
    }
});