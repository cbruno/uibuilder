//= require ./AppKit.Control.js
//= require ../jquery/spinner.js

AppKit.Stepper = AppKit.Control.extend({
    classID: 'appkit-stepper',
    options: {
        tabindex: -1,
        min: 0,
        max: 100,
        step: 1,
        continuous: false
    },
    initialize: function (options) {
        this.$el.addClass('ui-stepper');
        var self = this;
        this._input = $('<input type="text">')
            .appendTo(this.$el)
            .spinner(_.extend({
                stop: function (event, ui) {

                    if (self.get('continuous')) {
                        self.fire();
                    }

                }
            }, options));

        this._input.on('keyup', function (evt) {
            if (evt.keyCode == $.ui.keyCode.ENTER) {
                self.fire();
            }
        }).on('blur', function () {
            if (!self.get('continuous')) {
                self.fire();
            }
        });

        this._super(options);
        this.set('value', this.get('min'));
    },
    min: function (aNumber) {
        if (aNumber !== undefined) {
            if (this._input) {
                this._input.spinner('option', 'min', aNumber);
            }
        }
        if (this._input) {
            return this._input.spinner('option', 'min');
        }

        return 0;

    },
    max: function (aNumber) {
        if (aNumber !== undefined) {
            if (this._input) {
                this._input.spinner('option', 'max', aNumber);
            }
        }
        if (this._input) {
            return this._input.spinner('option', 'max');
        }
        return 100;
    },
    step: function (aNumber) {
        if (aNumber !== undefined) {
            if (this._input) {
                this._input.spinner('option', 'step', aNumber);
            }
        }
        if (this._input) {
            return this._input.spinner('option', 'step');
        }
        return 5;
    },
    value: function (aNumber) {

        if (aNumber !== undefined) {
            if (this._input) {
                this._input.spinner('value', aNumber);
            }
        }
        if (this._input) {
            return this._input.spinner('value');
        }

        return undefined;
    },
    enabled: function (aBOOL) {
        var val = this._super(aBOOL);

        if (aBOOL !== undefined) {
            if (this._input) {
                this._input.spinner('option', 'disabled', !aBOOL);
            }
        }

        return val;
    }
});