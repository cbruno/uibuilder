//= require ./AppKit.Control.js
//= require ../jquery/button.js

AppKit.Button = AppKit.Control.extend({
    classID: 'appkit-button',
    options: {
        type: 'default',
        text: true
    },
    initialize: function (options) {

        this.$el.button(options);
        this._super(options);
    },
    events: {
        'click': 'fire'
    },
    type: function (aString) {

        if (aString !== undefined) {

            this.$el.removeClass('default');
            this.$el.removeClass('primary');
            this.$el.removeClass('danger');
            this.$el.addClass(aString);
            this._type = aString;
        }

        return this._type;
    },
    label: function (aString) {
        if (aString !== undefined && aString !== this.$el.button('option', 'label')) {
            this.$el.button('option', 'label', aString);
        }

        return this.$el.button('option', 'label');
    },
    enabled: function (aBool) {
        if (aBool !== undefined) {
            if (this.$el.button('instance')) {
                this.$el.button('option', 'disabled', !aBool);
            }
            this.$el.removeAttr('disabled');
        }
        return this._super(aBool);
    }
});