//= require ./AppKit.Control.js

AppKit.Form = AppKit.Control.extend({
    classID: 'appkit-form',
    options: {
        tabindex: -1
    },
    initialize: function (options) {
        this._super(options);
        this.$el.css('outline', 'none');
    },
    events: {
        'keyup': function (evt) {
            if (evt.keyCode == $.ui.keyCode.ENTER)
                this.fire();
        }
    }
});