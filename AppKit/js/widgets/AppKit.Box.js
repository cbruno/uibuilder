//= require ../core/AppKit.View.js


AppKit.Box = AppKit.View.extend({
    classID: 'appkit-box',
    options: {label: null},
    initialize: function (options) {

        this.$el.addClass("ui-box");
        //add label
        var back = $('<div>').addClass("ui-box-back")
            .appendTo(this.$el);

        $('<span>').addClass('ui-box-label').appendTo(back);
    },
    label: function (aString) {
        if (aString !== undefined) {
            this.$el.find('.ui-box-label').html(aString);
            this.$el.find('.ui-box-label').css({
                'background-color': this.$el.parent().css('background-color')
            });
        }

        return this.$el.find('.ui-box-label').text();
    }
})