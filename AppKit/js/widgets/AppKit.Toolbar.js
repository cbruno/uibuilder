//= require ./AppKit.Control.js

AppKit.ToolbarItem = AppKit.Control.extend({
    classID: 'appkit-toolbar-item',
    options: {
        label: null,
        active: true,
        selected: false,
        image: null
    },
    initialize: function (options) {

        this.$el.addClass('ui-toolbar-item');

        $('<div>').addClass("ui-toolbar-item-label")
            .appendTo(this.$el);

        this._super(options);
    },
    events: {
        mousedown: function (evt) {
            evt.preventDefault();

            if (!this.get('enabled'))
                return;

            if (this._active && evt.which < 2) {
                this.$el.addClass('ui-state-active');

            }
            var evtID = _.uniqueId('evt-');
            var self = this;
            $(document).on('mouseup.' + evtID, function (evt2) {
                self.$el.removeClass('ui-state-active');

                $(document).off('mouseup.' + evtID);
            });
        },
        mouseover: function () {
            this.$el.addClass('ui-state-hover');
        },
        mouseout: function () {
            this.$el.removeClass('ui-state-hover');
        },
        click: function (evt) {

            if (!this.get('enabled'))
                return;

            if (evt.which < 2)
                this.fire();

        },
        keydown: function (evt) {
            if (!this.get('enabled'))
                return;

            if (evt.which === $.ui.keyCode.SPACE) {

                if (this._active) {
                    this.$el.addClass("ui-state-active");
                    //self._swapInActiveImage();
                    this.fire();
                }

            }
        },
        keyup: function () {
            this.$el.removeClass('ui-state-active');
        }
    },
    active: function (aBool) {
        if (aBool === true) {
            this.$el.attr('role', 'button');
            this.$el.attr('tabindex', 0);
            this._active = true;
        }
        else if (aBool === false) {
            this.$el.attr('role', 'region');
            this.$el.attr('tabindex', -1);
            this._active = false;

        }
        return this._active;
    },
    selected: function (aBool) {
        if (aBool === true) {
            this.$el.addClass('ui-state-selected')
        }
        else if (aBool === false) {
            this.$el.removeClass('ui-state-selected');

        }
        return this.$el.hasClass('ui-state-selected');
    },
    label: function (aString) {
        if (aString !== undefined) {
            this.$el.find('.ui-toolbar-item-label').html(aString);
        }

        return this.$el.find('.ui-toolbar-item-label').text();
    }
});

AppKit.Toolbar = AppKit.View.extend({
    classID: 'appkit-toolbar',
    initialize: function (options) {

        this.$el.addClass('ui-toolbar');
        this.$el.attr('role', 'toolbar');

        var self = this;
        //get the parent window

        if (this.$el.parent().hasClass('uidialog')) {
            this.$el.parent().on('dialogresize', function () {
                self.layout();
            });
        }
        else //the parent window is the browser window
        {
            jQuery(window).on('resize', function (evt) {
                if (evt.target === window)
                    setTimeout(function () {
                        self.layout();
                    }, 0);
            });


        }

        //first toolbar item has left margin
        if (this.$el.children('.ui-toolbar-item').length) {
            this.$el.children('.ui-toolbar-item').eq(0).css("margin-left", 2);
        }

        this.layout();

    },
    layout: function () {
        var items = this.$el.children('.ui-toolbar-item, .ui-toolbar-item-separator');

        var totalWidth = this.$el.outerWidth() - 12;
        var count = items.length,
            i = 0;

        for (; i < count; i++) {
            var item = items.eq(i);
            totalWidth -= (item.outerWidth() + 2);
        }

        var flexers = this.$el.children('.ui-toolbar-item-flex'),
            flen = flexers.length;
        if (flen > 0) {
            var wsize = totalWidth / flen;
            i = 0;
            for (; i < flen; i++) {
                var flex = flexers.eq(i);
                flex.css("width", wsize);
            }
        }
    }
});