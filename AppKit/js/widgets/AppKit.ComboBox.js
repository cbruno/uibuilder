//= require ./AppKit.Control.js
//= require ./AppKit.Menu.js
//= require ../jquery/combobox.js


AppKit.ComboBox = AppKit.Control.extend({
    classID: 'appkit-combo',
    options: {
        placeholder: '',
        radio: false,
        tabindex: 0,
        menu : undefined
    },
    initialize: function (options) {
        
        this.$el.combobox();

        this._super(options);

        var self = this;
        this.input().bind('focus change keydown paste', function () {
            self.clearValidationWarning();
        });

    },
    events : {
        selection : 'fire',
        focus : function(){
            this.input().focus();
        }
    },
    input: function () {
        return this.$el.find('input');
    },
    configureMenu : function(config)
    {
         if(_.isObject(config)){
            var m = new AppKit.Menu(config);
            this.$el.combobox('setMenu', m.$el);
         }

    },
    selectedIndex: function (idx) {

        if (this.$el.combobox('instance')) {
            if (idx !== undefined) {
                this.$el.combobox('selected', idx);
            }

            return this.$el.combobox('selected');
        }

        return -1;
    },
    selectedValue: function (aString) {
        if (this.$el.combobox('instance')) {
            if (aString !== undefined)
                this.$el.combobox('selectedValue', aString);

            return this.$el.combobox('selectedValue');
        }
        return undefined;
    },
    placeholder: function (aString) {
        if (this.$el.combobox('instance')) {
            if (aString !== undefined) {
                this.$el.combobox('option', 'placeholder', aString);
            }
            return this.$el.combobox('option', 'placeholder');
        }
        return undefined;
    },
    enabled: function (aBool) {

        if (aBool === true) {
            if (this.$el.combobox('instance')) {
                this.$el.combobox('option', 'enabled', true);
            }
            this.$el.attr('tabindex', 0);
        }
        else if (aBool === false) {
            if (this.$el.combobox('instance')) {
                this.$el.combobox('option', 'enabled', false);
            }
            this.$el.attr('tabindex', -1);
        }
        return !this.$el.hasClass('disabled');
    },
    radio: function (aBOOL) {
        if (this.$el.combobox('instance')) {
            if (aBOOL !== undefined) {
                this.$el.combobox('option', 'radio', aBOOL);
            }
            return this.$el.combobox('option', 'radio');
        }
        return false;
    },
    clearValidationWarning: function () {
        if (this.input().hasClass('error')) {
            this.input().removeClass('error');
            this.input().tooltip('destroy');
            this.input().removeAttr('title');
        }
    },
    validate: function (aValidator) {
        if (!_.isFunction(aValidator))
            return true;

        var message = aValidator(this.value());

        if (message) {
            if (this.input().hasClass('error'))
                this.input().tooltip('destroy');

            this.input().tooltip({
                content: message,
                items: 'input',
                show: {
                    duration: 0,
                    easing: false,
                    effect: null
                },
                position: { my: "left center",
                    at: "right+10 center",
                    collision: "flipfit",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                },
                tooltipClass: "left"
            });

            this.input().addClass('error');
            this.input().tooltip('open');

            return false;
        }

        return true;
    },
    value: function (aValue) {
        if (this.$el.combobox('instance')) {
            
			if (aValue !== undefined) {
                this.$el.combobox('value', aValue);
                this.clearValidationWarning();
            }

            return this.$el.combobox('value');
        }
        return null;
    }
});