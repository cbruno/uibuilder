//= require ../core/AppKit.View.js
//= require ../jquery/menu.js

AppKit.MenuItem = AppKit.Control.extend({
    classID: 'appkit-menu-item',
    options: {
        tabindex: null
    },
    initialize: function (options) {
        this._super(options);
        var self = this;
        this.$el.on('fire', function () {
            self.fire();
        });
    },
    label: function (aString) {
        if (aString !== undefined) {
            this.$el.html(aString);
        }

        return this.$el.text();
    }
});


AppKit.Menu = AppKit.View.extend({
    classID: 'appkit-menu',
    options: {hidden: true},
    initialize: function (options) {

        this._manualSetup(options);

        this.$el.menu();
    },
    _manualSetup: function (options) {
        if (options.items !== undefined
            && options.items.length) {
            var count = options.items.length,
                i = 0;

            for (; i < count; i++) {
                $('<div class="appkit-menu-item">')
                    .attr(options.items[i])
                    .awaken()
                    .appendTo(this.$el);

            }
        }
    },
    visible: function (aBool) {
        if (aBool === true) {
            this.open();
        }
        else if (aBool === false) {
            this.close();
        }

        return this.$el.is(':visible');
    },
    open: function () {
        this.$el.menu('open');
    },
    close: function () {
        this.$el.menu('close');
    },
    toggle: function () {
        if (this.get('visible'))
            this.close();
        else
            this.open();
    },
    position: function (pos) {
        if (pos !== undefined) {
            this.$el.offset(pos);
        }

        return this.$el.offset();
    }
});