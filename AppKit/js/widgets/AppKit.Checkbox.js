//= require ./AppKit.Control.js

AppKit.CheckBox = AppKit.Control.extend({
    classID: 'appkit-checkbox',
    options: {
        checked: true,
        label: null
    },
    initialize: function (options) {

        this.$el.addClass("ui-checkbox");
        this.$el.attr("role", "checkbox");

        if (!options.label) {
            this.options.label = this.$el.html();
        }
        this.$el.empty();

        var cell = $("<div class='ui-checkbox-button-cell'>");

        this._checkKnob = $("<div>").addClass('ui-checkbox-bezel');
        cell.append(this._checkKnob);
        cell.append($("<div class='ui-checkbox-label'></div>"));

        this.$el.append(cell);

        this._super(options);
    },
    events: {
        click: function (evt) {
            if (!this.$el.hasClass("disabled") && evt.which < 2) {
                this.toggle();
            }
        },
        focus: function () {
            this.$el.addClass('ui-state-focus');
        },
        blur: function () {
            this.$el.removeClass('ui-state-focus');
        },
        mousedown: function (evt) {
            evt.preventDefault();
        },
        keydown: function (evt) {
            if (!this.$el.hasClass("disabled")) {
                if (evt.which === $.ui.keyCode.SPACE) {
                    this.toggle();
                }
            }
        }
    },
    toggle: function () {
        if (this.get('checked'))
            this.set('checked', false);
        else
            this.set('checked', true)
    },
    checked: function (aBOOL) {
	 	
		if (this._checkKnob) {
            
			if (aBOOL === undefined){
				return this._checkKnob.hasClass("icon-check");
			}
            if (aBOOL === true) {
                this._checkKnob.addClass("icon-check");

            }
            else {
                this._checkKnob.removeClass("icon-check");
            }

            this.fire();
        }
    },
    label: function (aString) {
        if (aString !== undefined) {
            this.$el.find('.ui-checkbox-label').html(aString);
        }

        return this.$el.find('.ui-checkbox-label').text();
    },
    value: function (aBOOL) {
        console.log('calling value')
		return this.set('checked', aBOOL);
    }
});