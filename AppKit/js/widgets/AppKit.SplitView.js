//= require ../core/AppKit.View.js
//= require ../jquery/splitview.js

AppKit.SplitView = AppKit.View.extend({
    classID: 'appkit-splitview',
    options: {
        delegate: null,
        dividerThickness: 10,
        orientation: 'horizontal',
        fixed: false
    },
    initialize: function (options) {
        this.$el.splitview(options);
    },
    events: {
        sizer: function () {
            var theDelegate = this.get('delegate');
            if (theDelegate) {
                if (_.isFunction(theDelegate.onSplitterSizing)) {
                    theDelegate.onSplitterSizing(this);
                }
            }
        }
    },
    delegate: function (aDelegate) {
        if (aDelegate !== undefined) {
            this._delegate = AppKit.get(aDelegate);
        }

        return this._delegate;
    },
    dividerThickness: function (aNumber) {
        if (aNumber !== undefined) {
            this.$el.splitview('option', 'dividerThickness', aNumber);
        }

        return this.$el.splitview('option', 'dividerThickness');
    },
    orientation: function (aString) {
        if (aString !== undefined) {
            this.$el.splitview('option', 'orientation', aString);
        }

        return this.$el.splitview('option', 'orientation');
    },
    fixed: function (aBOOL) {
        if (aBOOL !== undefined) {
            this.$el.splitview('option', 'fixed', aBOOL);
        }

        return this.$el.splitview('option', 'fixed');
    },
    splitterPosition: function (aNumber) {
        if (aNumber !== undefined) {
            this.$el.splitview('splitterPosition', aNumber);
        }

        return this.$el.splitview('splitterPosition');
    }
});
