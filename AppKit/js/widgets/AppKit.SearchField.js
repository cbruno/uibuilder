//= require ./AppKit.Control.js


AppKit.SearchField = AppKit.Control.extend({
    classID: 'appkit-searchfield',
    options: {placeholder: null, tabindex: -1},
    initialize: function (options) {

        this.$el.addClass('ui-searchfield');

        this._input = $('<input type="text">').appendTo(this.$el);

        this._super(options);

        var rm = false;
        if (this.$el.height() === 0) {
            this.$el.css('visibility', 'hidden');
            this.$el.appendTo($('body'));
            rm = true;
        }


        $('<div class="icon-search">').appendTo(this.$el)
            .css('top', (this.$el.height() - 14) / 2.0);

        var unique = _.uniqueId('cancel-search-');
        var self = this;
        var cancel = $('<div class="icon-cancel-circle">')
            .appendTo(this.$el)
            .on('mousedown', function (evt) {
                evt.preventDefault();
                $(this).addClass('ui-state-active');
                var that = $(this);
                $(document).on('mouseup.' + unique, function () {
                    that.removeClass('ui-state-active');
                    $(document).off('mouseup.' + unique);
                });
            })
            .on('click', function () {
                self._input.val('');
                $(this).hide();
            })
            .css('top', (this.$el.height() - 14) / 2.0);

        this._input.on('focus change keydown paste', function () {
            var that = $(this);
            setTimeout(function () {
                if (that.val().length) {
                    cancel.show();
                }
                else {
                    cancel.hide();
                }
            }, 0);
        }).on('change keydown paste', function () {
            if (self._delegate) {
                if (_.isFunction(self._delegate.textDidChange)) {
                    self._delegate.textDidChange.call(self._delegate, self);
                }
            }
        });

        if (rm) {
            this.$el.detach();
            this.$el.css('visibility', 'visible');
        }
    },
    input: function () {
        return this._input;
    },
    enabled: function (aBool) {
        if (aBool === true) {
            this._input.removeClass('disabled');
            this._input.attr('tabindex', 0);
            this._input.removeAttr('readonly');
        }
        else if (aBool === false) {
            this._input.addClass('disabled');
            this._input.attr('tabindex', -1);
            this._input.attr('readonly', 'true');
        }
        return this._input.hasClass('disabled');
    },
    placeholder: function (aString) {
        if (aString !== undefined) {
            this._input.attr('placeholder', aString);
        }

        return this._input.attr('placeholder');
    },
    value: function (aValue) {
        if (aValue !== undefined) {
            this._input.val(aValue);
        }

        return this._input.val();
    }});