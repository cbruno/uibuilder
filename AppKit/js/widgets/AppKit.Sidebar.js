//= require ../jquery/w2sidebar.js

AppKit.Sidebar = AppKit.View.extend({
	classID : 'appkit-sidebar',
	initialize : function(){
		
		var nodes = this._html2Nodes(this.$el);
		var self = this; 
		this.$el.w2sidebar({
		        name: this.id,
				nodes : nodes,
				onDblClick: function(evt) {
				    var delegate = self.get('delegate');
					if(delegate){
						if(_.isFunction(delegate.onDblClick)){
							delegate.onDblClick.call(delegate, self, evt.target, evt);
						}
					}     
				},
				onClick: function(evt) {
				    var delegate = self.get('delegate');
					if(delegate){
						if(_.isFunction(delegate.onClick)){
							delegate.onClick.call(delegate, self, evt.target, evt);
						}
					}     
				},
				onExpand: function(evt) {
				    var delegate = self.get('delegate');
					if(delegate){
						if(_.isFunction(delegate.onExpand)){
							delegate.onExpand.call(delegate, self, evt.target, evt);
						}
					}     
				},
				onCollapse: function(evt) {
				    var delegate = self.get('delegate');
					if(delegate){
						if(_.isFunction(delegate.onCollapse)){
							delegate.onCollapse.call(delegate, self, evt.target, evt);
						}
					}     
				} 
		});
	},
	delegate: function (aDelegate) {
        if (aDelegate !== undefined) {
            this._delegate = AppKit.get(aDelegate);
        }

        return this._delegate;
    },
	expand : function(id){
		w2ui[this.id].expand(id);
	},
	expandAll : function(id){
		w2ui[this.id].expandAll(id); 
	},
	collapse : function(id){
		w2ui[this.id].collapse(id); 
	},
	collapseAll : function(id){
		w2ui[this.id].collapseAll(id); 
	},
	_html2Nodes : function(parent){
		var self = this,
			nodes = [];
		parent.children('.appkit-sidebar-node').each(function(idx, node){
			var attr = $(node).data();
			if(attr.id === undefined){
				attr.id = _.uniqueId(self.id+'-node-');
			} 
			 
			attr.nodes = self._html2Nodes($(node));	
			nodes.push(attr);
		});
		
		return nodes; 
	}
});