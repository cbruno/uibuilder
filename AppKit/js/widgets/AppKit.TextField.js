//= require ./AppKit.Control.js
//= require ../jquery/tooltip.js

AppKit.TextField = AppKit.Control.extend({
    classID: 'appkit-textfield',
    options: {tabindex: -1},
    initialize: function (options) {

        this.$el.addClass('ui-textfield');
        var self = this;
        this._input = $('<input type="text" />');

        this._input.on('focus change keydown paste', function () {
            self.clearValidationWarning();
        }).on('change keydown paste', function () {
            if (self._delegate) {
                if (_.isFunction(self._delegate.textDidChange)) {
                    self._delegate.textDidChange.call(self._delegate, self);
                }
            }
        });

        this._input.appendTo(this.$el);
        this._super(options);

    },
    input: function () {
        return this._input;
    },
    clearValidationWarning: function () {
        if (this.input().hasClass('error')) {
            this.input().removeClass('error');
            this.input().tooltip('destroy');
            this.input().removeAttr('title');
        }
    },
    enabled: function (aBool) {
        if (aBool === true) {
            this._input.removeClass('disabled');
            this._input.attr('tabindex', 0);
            this._input.removeAttr('readonly');
        }
        else if (aBool === false) {
            this._input.addClass('disabled');
            this._input.attr('tabindex', -1);
            this._input.attr('readonly', 'true');
        }
        return this._input.hasClass('disabled');
    },
    validate: function (aValidator) {
        if (!_.isFunction(aValidator))
            return true;

        var message = aValidator(this.value());

        if (message) {
            if (this.input().hasClass('error'))
                this.input().tooltip('destroy');

            this.input().tooltip({
                content: message,
                items: 'input',
                show: {
                    duration: 0,
                    easing: false,
                    effect: null
                },
                position: { my: "left center",
                    at: "right+10 center",
                    collision: "flipfit",
                    using: function (position) {
                        $(this).css(position);
                        $('<div>')
                            .addClass("arrow").appendTo(this);
                    }
                },
                tooltipClass: "left"
            });


            this.input().addClass('error');
            this.input().tooltip('open');

            return false;
        }

        return true;
    },
    value: function (aValue) {
        if (aValue !== undefined) {
            this.input().val(aValue);
            this.clearValidationWarning();
        }

        return this.input().val();
    }
});