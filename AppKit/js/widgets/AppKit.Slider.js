//= require ./AppKit.Control.js


AppKit._SliderHandle = AppKit.View.extend({
    slider: null,
    initialize: function (options) {
        
        this.$el.addClass('ui-slider-handle').attr('tabindex', 0);
        this._startValue = 0.0;

        this.set('position', 0);

        this._bindEvents();


    },
    _bindEvents: function () {
        this._eventID = _.uniqueId('ui-slider-handle-');
        var self = this;
        this.$el.on('mousedown', function (evt) {

            if (self.slider) {

                if (!self.slider.get('enabled'))
                    return;

                if (evt.which < 2) {

                    evt.preventDefault();
                    evt.stopPropagation();
                    self._startValue = self.slider.get('value');
                    self.slider._leftMouseDown = true;
                    self._leftMouseDown = true;
                    self.makeActive(true);

                    document.onselectstart = function () {
                        return false;
                    };

                    $("body").css("cursor", "default");

                    $(document).on('mousemove.' + self._eventID, function (evt2) {

                        if (self.slider) {
                            if (self._leftMouseDown && self.slider._activeHandle) {
                                var oldValue = self.slider.get('value');
                                self.slider.calcValue(evt2);
                                if (self.slider.get('continuous') && oldValue != self.slider.get('value'))
                                    self.slider.fire();
                            }
                        }
                    }).on('mouseup.' + self._eventID, function (evt3) {
                        if (self.slider) {
                            if (self._leftMouseDown && !self.slider.continuous &&
                                self._startValue !== self.slider.get('value')) {
                                self.slider.fire();
                            }
                        }

                        $("body").css("cursor", "inherit");
                        document.onselectstart = function () {
                            return true;
                        };

                        self._leftMouseDown = false;

                        $(document).off('mousemove.' + self._eventID + ", mouseup." + self._eventID);
                    });

                }
            }

        }).on('focus', function () {
            $(this).addClass('ui-state-focus');
            self.makeActive(true);
        }).on('blur', function () {
            $(this).removeClass('ui-state-focus');
        });

    },
    makeActive: function (aBool) {
        if (this.slider) {
            if (aBool) {
                if (this.slider._activeHandle) {
                    this.slider._activeHandle.makeActive(false);
                }

                this.slider._activeHandle = this;

                this.$el.addClass("selected");
                this.$el.css("zIndex", 3);

                this.slider.set('value', this.slider.valueOfKnobPosition(this.get('position')));

            }
            else {
                this.$el.removeClass("selected");
                this.$el.css("zIndex", 2);
            }
        }
    }
});

AppKit.Slider = AppKit.Control.extend({
    classID: 'appkit-slider',
    step: 1,
    minValue: 0,
    maxValue: 100,
    options: {
        tabindex: -1,
        continuous: false,
        vertical: false,
        step: 1.0,
        minValue: 0,
        maxValue: 100,
        type: 'linear'
    },
    initialize: function (options) {

        this.$el.attr("role", "ui-slider");
        this._rangeDiv = null;
        this._leftMouseDown = false;
        this._handles = [];

        this._super(options);

        this.$el.attr("aria-valuemin", this.get('minValue'));
        this.$el.attr("aria-valuemax", this.get('maxValue'));
        this.$el.attr("aria-valuenow", this.get('minValue'));


        var self = this;
        if (this.get('type') === "circular") {
            this.$el.addClass("ui-circular-slider")
                .attr('tabindex', 0);
            this._activeHandle = null;
            this._handles = null;
            this._cknob = $("<div>")
                .addClass("circular-knob")
                .appendTo(this.$el);

            this.$el.css({
                width: 28.0,
                height: 28.0
            });


            this.set('value', this.get('minValue'));
        }
        else {

            this.$el.addClass("ui-slider");

            this._activeHandle = new AppKit._SliderHandle({
                slider: self
            });

            this._addHandle(this._activeHandle);

            if (this.get('vertical')) {
                this.$el.addClass("ui-slider-vertical");
            }
            else {
                this.$el.addClass("ui-slider-horizontal");
            }

            this.set('value', this.get('minValue'));

            this._positionKnob();

        }

        this._bindEvents();

    },
    calcValue: function (anEvent) {
        
        if (this.get('type') === "circular") {
            var offsetx = anEvent.pageX - this.$el.offset().left,
                offsety = anEvent.pageY - this.$el.offset().top;

            var x = offsetx - 14,
                y = offsety - 14.0,
                angle = Math.atan2(y, x) - Math.PI / 2.0;

            var dv = this.valueOfKnobPosition(-angle);

            this.set('value', dv);

        }
        else {
            var offsetx = anEvent.pageX - this.$el.offset().left,
                offsety = anEvent.pageY - this.$el.offset().top;

            if (this.get('vertical')) {
                var dv = this.valueOfKnobPosition(offsety);
                this.set('value', dv);
            }
            else {
                var dv = this.valueOfKnobPosition(offsetx);
                this.set('value', dv);
            }

        }
    },
    value: function (v) {

        if (v !== undefined) //set
        {
            if (this._activeHandle && this._handles.length > 1) {
                if (this._activeHandle === this._handles[0]) {

                    if (v > this.valueOfKnobPosition(this._handles[1].get('position'))) {
                        return;
                    }
                }
                else if (this._activeHandle === this._handles[1]) {
                    if (v < this.valueOfKnobPosition(this._handles[0].get('position'))) {
                        return;
                    }
                }
            }

            var dv = parseFloat("" + v);

            dv = this.stepToNearestAllowedValue(dv);
            this._sliderValue = dv; 
            this.$el.attr('aria-valuenow', this._sliderValue);
            this._positionKnob();

        }

        return this._sliderValue;

    },
    valueOfKnobPosition: function (pos) {
        if (this.get('type') === "circular") {
            var p = 0.0;

            if (pos >= 0) {
                p = pos / (2.0 * Math.PI);
            } else {
                p = (2.0 * Math.PI + pos) / (2.0 * Math.PI);
            }

            var dv = p * (this.get('maxValue') - this.get('minValue')) + this.get('minValue');
            dv = this.stepToNearestAllowedValue(dv);

            return dv;
        }
        else {
            var p = 0.0;
            if (this.get('vertical')) {
                var h = this.$el.height() - 20;
                p = Math.max(0.0, Math.min(1.0, (h - (pos - 10 )) / h));
            } else {
                p = Math.max(0.0, Math.min(1.0, (pos - 10) / (this.$el.width() - 20)));
            }

            var dv = p * (this.get('maxValue') - this.get('minValue')) + this.get('minValue');

            dv = this.stepToNearestAllowedValue(dv);

            return dv;
        }

    },
    resizeSubviewsWithOldSize: function (oldSize) {
        this._positionKnob();
    },
    stepToNearestAllowedValue: function (dv) {
        var m = Math.round(dv / this.get('step'))
        return Math.min(this.get('maxValue'), Math.max(this.get('minValue'), m * this.get('step')));
    },
    _positionKnob: function () {

        var p = 1 - (this.get('maxValue') - this.get('value')) / (this.get('maxValue') - this.get('minValue'));

        if (this.get('type') === "circular") {
            var angle = (1.0 - p) * 2.0 * Math.PI + Math.PI / 2.0;
            this._positionKnobAtAngle(angle);
        }
        else {

            if (this._activeHandle) {
                if (this.get('vertical')) {


                    var rm = false;
                    if (this.$el.height() === 0) { // need to get height
                        this.$el.css('visibility', 'hidden');
                        this.$el.appendTo($('body'));
                        rm = true;
                    }

                    var yshift = Math.round((1 - p) * (this.$el.height() - 20 ));

                    if (rm) {
                        this.$el.detach();
                        this.$el.css('visibility', 'visible');
                    }


                    this._activeHandle.$el.css("top", yshift + 10);
                    this._activeHandle.set('position', yshift + 10);
                }
                else {
                    if (this._activeHandle) {
                        var rm = false;
                        if (this.$el.width() === 0) {
                            this.$el.css('visibility', 'hidden');
                            this.$el.appendTo($('body'));
                            rm = true;
                        }

                        var xshift = Math.round(p * (this.$el.width() - 20));

                        if (rm) {
                            this.$el.detach();
                            this.$el.css('visibility', 'visible');
                        }

                        this._activeHandle.$el.css("left", xshift + 10);
                        this._activeHandle.set('position', xshift + 10);

                    }

                }
            }
        }
    },
    _positionKnobAtAngle: function (angle) {
        var shift = 10.5;

        var x = 10.0 * Math.cos(angle) + shift;
        var y = 10.0 * Math.sin(angle) + shift;

        if (this._cknob) {
            this._cknob.css({
                left: x,
                top: y
            });
        }
    },
    _addHandle: function (aSliderHandle) {
        this._handles.push(aSliderHandle);
        this.$el.append(aSliderHandle.$el);
    },
    _bindEvents: function () {
        this._eventID = _.uniqueId('ui-slider-event-');
        var self = this;
        this.$el.bind({
            mousedown: function (evt) {

                evt.preventDefault();

                if (!self.get('enabled'))
                    return;

                if (evt.which < 2) {


                    self.calcValue(evt);

                    self._leftMouseDown = true;
                    document.onselectstart = function () {
                        return false;
                    };
                    $("body").css("cursor", "default");

                    self.fire();

                    $(document).on('mousemove.' + self._eventID, function (evt2) {
                        if (self._leftMouseDown) {
                            if (self.get('type') === "circular") {
                                var oldValue = self.get('value');
                                self.calcValue(evt2);
                                if (self.get('continuous') && oldValue !== self.get('value'))
                                    self.fire();
                            }

                        }
                    }).on('mouseup.' + self._eventID, function (evt3) {
                        if (self.get('type') === "circular") {
                            if (self._leftMouseDown && !self.get('continuous'))
                                self.fire();

                            document.onselectstart = function () {
                                return true;
                            };

                            self._leftMouseDown = false;
                            $("body").css("cursor", "inherit");

                            $(document).off('mousemove.' + self._eventID + ", mouseup." + self._eventID);
                        }
                    });
                }

            },
            focus: function () {
                $(this).addClass('ui-state-focus');
            },
            blur: function () {
                $(this).removeClass('ui-state-focus');
            },
            keydown: function (evt) {

                if (!self.get('enabled'))
                    return;

                var KC = evt.which;

                if (self.get('type') === "circular") {
                    if (KC === $.ui.keyCode.RIGHT || KC === $.ui.keyCode.LEFT) {
                        var dv = self.get('value') + self.get('step');
                        if (KC === $.ui.keyCode.LEFT)
                            dv = self.get('value') - self.get('step');

                        dv = self.stepToNearestAllowedValue(dv);
                        self.set('value', dv);
                        self.fire();
                    }
                }
                else {


                    if (self.get('vertical')) {
                        if (KC === $.ui.keyCode.UP || KC === $.ui.keyCode.DOWN) {

                            var dv = self.get('value') + self.get('step');

                            if (KC === $.ui.keyCode.DOWN)
                                dv = self.get('value') - self.get('step');

                            dv = self.stepToNearestAllowedValue(dv);
                            self.set('value', dv);
                            self.fire();
                        }
                    }
                    else {
                        if (KC === $.ui.keyCode.RIGHT || KC === $.ui.keyCode.LEFT) {

                            var dv = self.get('value') + self.get('step');

                            if (KC === $.ui.keyCode.LEFT)
                                dv = self.get('value') - self.get('step');

                            dv = self.stepToNearestAllowedValue(dv);
                            self.set('value', dv);
                            self.fire();

                        }
                    }
                }
            }

        });
    }

});