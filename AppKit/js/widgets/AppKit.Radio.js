//= require ./AppKit.Control.js

AppKit.RadioGroup = AppKit.Control.extend({
    classID: "appkit-radio-group",
    radios: [],
    options: {
        tabindex: -1
    },
    initialize: function (options) {

        this.$el.addClass("ui-radio-group");
        this._super(options);

    },
    selected: function (aRadio) {
        if (aRadio !== undefined) {
            this._selected = aRadio;
            if (this._selected) {
                this._selected.set('selected', true);
            }
        }
        return this._selected;
    },
    enabled: function (aBOOL) {

        var val = this._super(aBOOL);

        if (aBOOL !== undefined) {
            var count = this.radios.length,
                i = 0;

            for (; i < count; i++) {
                this.radios[i].set('enabled', aBOOL);
            }
        }

        return val;
    }
});

AppKit.RadioButton = AppKit.Control.extend({
    classID: "appkit-radio",
    options: {
        label: null,
        selected: false
    },
    initialize: function (options) {

        this.$el.addClass("ui-radio-button");
        this.$el.attr("role", "radio");

        if (!options.label) {
            options.label = this.$el.html();
        }

        this.$el.empty();

        var cell = $("<div class='ui-radio-button-cell'></div>");

        this._radioKnob = $("<div>").addClass('ui-radio');
        this._radioSelect = $("<div>").addClass("ui-radio-inner")
        this._radioKnob.append(this._radioSelect);

        cell.append(this._radioKnob);
        cell.append($("<div class='ui-radio-label'></div>"));

        this.$el.append(cell);
        this._super(options);

    },
    events: {
        click: function (evt) {
            if (this.get('enabled') && evt.which < 2) {
                this.select();
            }
        },
        mousedown: function (evt) {
            evt.preventDefault();
        },
        focus: function () {
            this.$el.addClass('ui-state-focus');
        },
        blur: function () {
            this.$el.removeClass('ui-state-focus');
        },
        keydown: function (evt) {
            if (this.get('enabled')) {
                if (evt.which == $.ui.keyCode.SPACE) {
                    this.select();
                }
            }
        }
    },
    onAwake: function () {

        this._radioGroup = this.$el.parent().widget();

        if (this._radioGroup && this._radioGroup.radios) {
            this._radioGroup.radios.push(this);
            this.set('enabled', this._radioGroup.get('enabled'));
        }
        else {
            this._radioGroup = null;
        }
    },
    selected: function (aBOOL) {

        if (aBOOL === undefined)
            return this.$el.hasClass("selected");

        if (this._radioGroup && aBOOL) {
            this._radioGroup.$el.children('.ui-radio-button').removeClass('selected');
        }

        if (aBOOL) {
            this.$el.addClass("selected");
            this.$el.trigger("selected");

            if (this._radioGroup) {
                this._radioGroup.fire();
            }
        }
        else {
            this.$el.removeClass("selected");
        }

    },
    select: function () {
        this.selected(true);
    },
    label: function (aString) {
        if (aString !== undefined) {
            this.$el.find('.ui-radio-label').html(aString);
        }

        return this.$el.find('.ui-radio-label').text();
    },
	value : function(aBool){
		return this.set('selected', aBool);
	}
});