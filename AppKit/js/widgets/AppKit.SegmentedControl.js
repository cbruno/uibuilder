//= require ./AppKit.Control.js
//= require ./AppKit.Button.js

AppKit.Segment = AppKit.Button.extend({
    classID: "appkit-segment",
    _segmentedControl: null,
    _widthPercent: 0,
    initialize: function (options) {

        this.$el.addClass("ui-segment");
        this._super(options);
    },
    onAwake: function () {

        this._segmentedControl = this.$el.parent().widget();
        if (this._segmentedControl) {
            this._segmentedControl.segments.push(this);
            this.set('enabled', this._segmentedControl.get('enabled'))
        }

    },
    fire: function () {

        if (this._segmentedControl.get('mode') === 'any') {
            this._segmentedControl.selectSegment(this.$el.index('.ui-segment'), !this.$el.hasClass('primary'));
        }
        else {
            this._segmentedControl.selectSegment(this.$el.index('.ui-segment'), true);
        }

        this._super();
    }
});


AppKit.SegmentedControl = AppKit.Control.extend({
    classID: "appkit-segmented-control",
    segments: [],
    options: {
        tabindex: -1,
        mode: 'one' //can be "one", "any" or "momentary"
    },
    initialize: function (options) {
        this.$el.addClass('ui-segmented-control');

        var segments = this.$el.children(".ui-segment");
        if (segments.length) {
            segments.eq(0).addClass('first');
            segments.eq(segments.length - 1).addClass('last');
        }

        this._super(options);
    },
    onAwake: function () {
        if (this.get('mode') === 'one') {
            if (this.segments.length) {
                this.selectSegment(0, true);
            }
        }
    },
    selectSegment: function (anIndex, aBOOL) {

        if (anIndex < this.segments.length) {
            if (this.get('mode') === "one" && aBOOL) {

                var count = this.segments.length,
                    i = 0;

                for (; i < count; i++) {
                    this.segments[i].$el.removeClass('selected');
                }

                if (aBOOL) {
                    this.segments[anIndex].$el.addClass('selected');
                }
                else {
                    this.segments[anIndex].$el.removeClass('selected');
                }
            }
            else if (this.get('mode') === "any") {
                if (aBOOL) {
                    this.segments[anIndex].$el.addClass('selected');
                } else {
                    this.segments[anIndex].$el.removeClass('selected');
                }
            }

            this.fire();
        }
    },
    selectedSegments: function () {
        var segments = this.$el.children(".ui-segment");
        if (this.get('mode') === "any") {
            var sel = [];
            $.each(segments, function (idx, selEl) {
                if ($(selEl).hasClass("selected")) {
                    sel.push(idx);
                }
            });

            return sel;
        }

        else if (this.get('mode') === "one") {
            var seg = this.$el.children(".selected");
            return seg.index();
        }

        return -1;

    },
    enabled: function (aBOOL) {
        var val = this._super(aBOOL);

        if (aBOOL !== undefined) {
            var count = this.segments.length,
                i = 0;

            for (; i < count; i++) {
                this.segments[i].set('enabled', aBOOL);
            }
        }

        return val;
    }

});

