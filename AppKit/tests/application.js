//= require ../js/AppKit.js

AppKit.Tests = {};

var  menu = null; 

AppKit.Tests.BasicTestsController = {
	buttonClick : function(sender)
	{
		alert(sender.get('label'))
	},
	onDblClick : function(sidebar, nodeid){
		console.info(nodeid);
	},
	showMenu : function(sender)
	{
		 this.combo.configureMenu({ items : [
			{
				'data-label' : 'Chicago', 
				'data-action' : 'AppKit.Tests.BasicTestsController.buttonClick'
			},
			{
				'class' : 'appkit-menu-item-separator'
			},
			{
				'data-label' : 'Los Angeles'
			},
			{
				'data-label' : 'New York'
			}
		]});
	},
	onSlider : function(sender)
	{
		this.stepper.set('value', sender.get('value'));
	},
	onStepper : function(sender)
	{
		this.slider.set('value', sender.get('value'));
	},
	onDialogOpen : function(sender)
	{
		sender.center();
	},
	onTest : function(sender)
	{
		 
	 	alert(this.myCheckbox.get('value')); 
	}
};





 


