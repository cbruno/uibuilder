#!/usr/bin/env node
'use strict';


var fs = require('fs-extra');
var path = require('path'); 
var builder = require('./build');
var Mincer  = require('mincer');


var environment = new Mincer.Environment();
environment.appendPath('./');


fs.writeFileSync('application.html', builder.createHostFile(environment), 'utf-8');

