#!/usr/bin/env node
'use strict';
 
var express = require('express');
var builder = require('./build.js');
var Mincer  = require('mincer');

exports.createAppServer = function(indexFilePath)
{
    var environment = new Mincer.Environment();
    environment.appendPath('./');

    if(indexFilePath){
      environment.indexFilePath = indexFilePath;
    }

    var app = express()

    app.use('./', Mincer.createServer(environment));
    app.use(express.static('./'));

    app.get('/', function (req, res) {

        var data = ""; 
        try {
            data = builder.createHostFile(environment);
        } catch (err) {
            console.error("Cannot create host file:");
            console.error(err.stack)
            throw err;
        }
        res.send(data)
    });

    return app; 
};
 

 




