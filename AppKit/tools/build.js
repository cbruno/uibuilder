var Mincer = require('mincer');
var Hogan = require('hogan.js');
var fs = require('fs-extra');
var path = require('path'); 

//remove the build directory if it exists
/* var rmdir = function(dir) {
  var list = fs.readdirSync(dir);
  for(var i = 0; i < list.length; i++) {
    var filename = path.join(dir, list[i]);
    var stat = fs.statSync(filename);
    
    if(filename == "." || filename == "..") {
      // pass these files
    } else if(stat.isDirectory()) {
      // rmdir recursively
      rmdir(filename);
    } else {
      // rm fiilename
      fs.unlinkSync(filename);
    }
  }
  fs.rmdirSync(dir);
}; */


exports.createHostFile = function(env)
{
    var str = '<!DOCTYPE html>\n<html>\n<head>\n\n'


    if(!env.indexFilePath)
    {
        env.indexFilePath = './main'
    }


    //add scripts
     
    var seenPaths = [];
    //javascript
    str = scriptInDependencies(path.dirname(env.indexFilePath)+'/application.js', str, env, seenPaths);
   
    //css
    seenPaths = [];
    str = linkInDependencies(path.dirname(env.indexFilePath)+'/style.css', str, env, seenPaths);
   

    str+='</head>\n<body role="application">\n';
 
    //html

    str+=renderHTMLFile(env.indexFilePath);

    str+='\n</body>\n</html>';

    return str; 
};

function renderHTMLFile(filename)
{
    try{
        var str = fs.readFileSync(filename+".html", 'utf-8');
    }catch(e){
        console.log('Could not find HTML file: %s', filename+".html");
        return "";
    }

    var tags = getFileTagsFromHTML(str),
        count = tags.length,
        i = 0;
 
    if(count === 0){ 
       return str;
    }

    var data = {};
    for(; i < count; i++)
    {
       data[tags[i]] = renderHTMLFile(tags[i]);
    }
    
    return Hogan.compile(str, {delimiters: '<% %>'}).render(data);
}


function getFileTagsFromHTML(html)
{
    var scan = Hogan.scan(html, '<% %>'),
        count = scan.length,
        i = 0;

    var tags = [];
    for(; i < count; i++)
    {
        var tag = scan[i].tag; 
        if(tag === '_v')
        {
            tags.push(scan[i].n);
        }
    }

    return tags; 
}

function linkInDependencies(thePath, str, env, seenPaths)
{
    
    var asset = env.findAsset(thePath); 
    var  dep = asset.dependencyPaths,
        count = dep.length;

    seenPaths.push(path.resolve(thePath));
    
    var i = 0;
    for(; i < count; i++)
    {
        var p = dep[i].pathname;
        if(seenPaths.indexOf(p) == -1)
        {
            str = linkInDependencies(p, str, env, seenPaths);
        }
    }

    return str + linkIn(thePath); 

}

function scriptInDependencies(thePath, str, env, seenPaths)
{
    var asset = env.findAsset(thePath);
    var dep = asset.dependencyPaths,
        count = dep.length;

    seenPaths.push(path.resolve(thePath));
    
    var i = 0;
    for(; i < count; i++)
    {
        var p = dep[i].pathname;
        if(seenPaths.indexOf(p) == -1)
        {
            str = scriptInDependencies(p, str, env, seenPaths);
        }
    }

    return str + scriptIn(thePath); 
}

function linkIn(thePath)
{
    return '<link rel="stylesheet" type="text/css" href="'+path.relative('./', thePath)+ '" />\n';
}

function scriptIn(thePath)
{
   return '<script type="text/javascript" src="'+path.relative('./', thePath)+'"></script>\n';
}



