#!/usr/bin/env node
'use strict';

var server = require('./server.js');
var path = require('path');

if(process.argv.length > 2)
{
	var TEST_FILE =  path.normalize(__dirname + '/../tests/'+process.argv[2]);
	var app = server.createAppServer(TEST_FILE);

	var server = app.listen(3000, function () {

	  var port = server.address().port
	  console.info('Running test at localhost:%s', port);
	  console.info('Press CTRL-C to exit.');

	});

}

