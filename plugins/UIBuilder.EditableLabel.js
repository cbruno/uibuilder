UIBuilder.EditableLabel = AppKit.View.extend({
	className : 'uibuilder-editable-label',
	editOptions : {
		leftResizer : true,
		rightResizer : true,
		topResizer : true,
		bottomResizer : true,
		topLeftResizer : true,
		bottomLeftResizer : true,
		topRightResizer : true,
		bottomRightResizer : true,
	},
	initialize : function(options){

		this.$el.html("Label");
	},
	edit : function(aValue)
	{
		if(aValue !== undefined){
			 this.$el.html(aValue);
		}

		return this.$el.text();
	}
});


UIBuilder.Components.register('label', {
	name : 'Label',
    description : 'Displays text that the user can select.',
    image : '<div class="uibuilder-label-icon">Label</div>', 
    cursorAt : {left : 25, top : 25}
}, UIBuilder.EditableLabel);