

UIBuilder.EditableSearchField = AppKit.SearchField.extend({
	className : 'uibuilder-editable-searchfield',
	editOptions : {
		leftResizer : true,
		rightResizer : true
	},
	initialize : function(options){
		this._super(options);

		this.input().hide();
		this.$el.append($('<div class="search-text">'));
	},
	edit : function(aValue){
		if(aValue !== undefined){
			this.$el.children('.search-text').text(aValue);

			if(aValue.length > 0)
			{
				this.$el.children('.icon-cancel-circle').show();
			}
			else {
				this.$el.children('.icon-cancel-circle').hide();
			}
		}



		return this.$el.children('.search-text').text();
	}
});


UIBuilder.Components.register('searchfield', {
        name : 'Search Field',
        description : 'Allows the user to input queries.',
        image : '<img src="Resources/searchfield.png" />',
        cursorAt : {left : 50, top : 12}
}, UIBuilder.EditableSearchField);