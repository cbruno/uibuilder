

UIBuilder.EditableTextField = AppKit.Control.extend({
	className : 'uibuilder-editable-textfield',
	options : {tabindex : -1},
	editOptions : {
		leftResizer : true,
		rightResizer : true
	},
	edit : function(aValue){
		if(aValue !== undefined){
			this.$el.text(aValue)
		}

		return this.$el.text();
	}

});


UIBuilder.Components.register('textfield', {
        name : 'Text Field',
        description : 'Allows the user to input text.',
        image : '<img src="Resources/textfield.png" />', 
        cursorAt : {left : 50, top : 12}
}, UIBuilder.EditableTextField);