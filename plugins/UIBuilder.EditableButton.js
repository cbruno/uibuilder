

UIBuilder.EditableButton = AppKit.Button.extend({
	className : 'uibuilder-editable-button',
	editOptions : {
		leftResizer : true,
		rightResizer : true
	},
	options : {
		label : 'Button'
	},
	edit : function(aValue)
	{
		if(aValue !== undefined){
			this.set('label', aValue);
		}

		return this.get('label');
	}
});


UIBuilder.Components.register('button', {
	name : 'Standard Button',
    description : 'Executes an action when clicked.',
    image : '<img src="Resources/button.png" />', 
    cursorAt : {left : 50, top : 14}
}, UIBuilder.EditableButton);