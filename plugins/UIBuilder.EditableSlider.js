

UIBuilder.EditableSlider = AppKit.Slider.extend({
	className : 'uibuilder-editable-slider',
	editOptions : {
		leftResizer : true,
		rightResizer : true,
		editValue : false
	},
	options : {
		value : 50
	}
});


UIBuilder.EditableVerticalSlider = AppKit.Slider.extend({
	className : 'uibuilder-editable-slider',
	editOptions : {
		topResizer : true,
		bottomResizer : true,
		editValue : false
	},
	options : {
		vertical : true,
		editValue : 50
	}
});

UIBuilder.Components.register('hSlider', {
        name : 'Horizontal Slider',
        description : 'Allows the user to choose from a range of values.',
        image : '<img src="Resources/hSlider.png" />', 
        cursorAt : {left : 50, top : 0}
}, UIBuilder.EditableSlider);

UIBuilder.Components.register('vSlider', {
        name : 'Vertical Slider',
        description : 'Allows the user to choose from a range of values.',
        image : '<img src="Resources/vSlider.png" />', 
        cursorAt : {left : 0, top : 50}
}, UIBuilder.EditableVerticalSlider);