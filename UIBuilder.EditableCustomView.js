 

UIBuilder.EditableCustomView = AppKit.View.extend({
	className : 'uibuilder-custom-view',
	editFrame : null,
	editOptions : {
		leftResizer : true,
		rightResizer : true,
		topResizer : true,
		bottomResizer : true,
		topLeftResizer : true,
		bottomLeftResizer : true,
		topRightResizer : true,
		bottomRightResizer : true,
		editValue : false
	},
	initialize : function(options){
		this._super(options);

		this.$el.css('zIndex', 1);
		this.$el.html('<div class="table-container"><div class="info-text">Custom View</div></div>');
	} 
});


UIBuilder.Components.register('view', {
	name : 'Custom View',
    description : 'Use the Custom View to create your own views.',
    image : '<div class="uibuilder-custom-view-icon">Custom View</div>', 
    cursorAt : {left : 75, top : 75}
}, UIBuilder.EditableCustomView);


